//
//  TpThreeXibTwo.h
//  News Feed
//
//  Created by 4-OM on 12/5/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TpThreeXibView1.h"
@protocol TpThreeXibTwoDelegate<NSObject>

-(void)didUpdateView:(UIView *)hideView;

@end
@interface TpThreeXibTwo : UIView
@property (weak,nonatomic) id <TpThreeXibTwoDelegate>delegate;
@property (strong, nonatomic) IBOutlet UIView *hidden;
@property (strong, nonatomic) IBOutlet UIView *subView;
@property (strong, nonatomic) IBOutlet UIImageView *imgInXibTwo;
@property (strong, nonatomic) IBOutlet UIButton *homeInXibTwo;
@property (strong, nonatomic) IBOutlet UIButton *recycleInXibTwo;
@property (strong, nonatomic) IBOutlet UIButton *editInXibTwo;
@property (strong, nonatomic) IBOutlet UIButton *locateInXibTwo;
@property (strong, nonatomic) IBOutlet UIButton *logoutInXibTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imgHomeXibTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imgRecycleXibTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imgEditXibTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imgMapXibTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imgLogoutXibTwo;
@property (strong, nonatomic) IBOutlet UILabel *labelHome;
@property (strong, nonatomic) IBOutlet UILabel *labelRecycle;
@property (strong, nonatomic) IBOutlet UILabel *labelEdit;
@property (strong, nonatomic) IBOutlet UILabel *labelLocation;
@property (strong, nonatomic) IBOutlet UILabel *labelLogout;
- (IBAction)homePress:(id)sender;


@end
