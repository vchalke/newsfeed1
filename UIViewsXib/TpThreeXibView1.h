//
//  TpThreeXibView1.h
//  News Feed
//
//  Created by 4-OM on 12/5/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TpThreeXibTwo.h"

@protocol TpThreeXibView1Delegate<NSObject>

-(void)didUpdate:(NSString *)cateText;

@end

@interface TpThreeXibView1:UIView
@property (weak,nonatomic) id <TpThreeXibView1Delegate>delegate;
@property (strong, nonatomic) IBOutlet UIView *tpThrView1;
@property (strong, nonatomic) IBOutlet UIButton *menuButtom;
- (IBAction)menuButonPress:(id)sender;

@end
