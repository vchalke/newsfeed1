//
//  ComboXibOne.h
//  News Feed
//
//  Created by 4-OM on 12/6/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ComboXibOneDelegate<NSObject>

-(void)didUpdateView;

@end
@interface ComboXibOne : UIView
@property (weak,nonatomic) id <ComboXibOneDelegate>delegate;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UIView *subView;
@property (strong, nonatomic) IBOutlet UIView *subInView;
@property (strong, nonatomic) IBOutlet UIImageView *imgInXibTwo;
@property (strong, nonatomic) IBOutlet UIButton *homeInXibTwo;
@property (strong, nonatomic) IBOutlet UIButton *recycleInXibTwo;
@property (strong, nonatomic) IBOutlet UIButton *editInXibTwo;
@property (strong, nonatomic) IBOutlet UIButton *locateInXibTwo;
@property (strong, nonatomic) IBOutlet UIButton *logoutInXibTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imgHomeXibTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imgRecycleXibTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imgEditXibTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imgMapXibTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imgLogoutXibTwo;
@property (strong, nonatomic) IBOutlet UILabel *labelHome;
@property (strong, nonatomic) IBOutlet UILabel *labelRecycle;
@property (strong, nonatomic) IBOutlet UILabel *labelEdit;
@property (strong, nonatomic) IBOutlet UILabel *labelLocation;
@property (strong, nonatomic) IBOutlet UILabel *labelLogout;
- (IBAction)menuButtPress:(id)sender;
- (IBAction)homeButtPress:(id)sender;
- (IBAction)recycleButtPress:(id)sender;
- (IBAction)editButtPress:(id)sender;
- (IBAction)locateButtPress:(id)sender;
- (IBAction)logoutButtPress:(id)sender;


@end
