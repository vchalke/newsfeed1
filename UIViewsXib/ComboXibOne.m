//
//  ComboXibOne.m
//  News Feed
//
//  Created by 4-OM on 12/6/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "ComboXibOne.h"

@implementation ComboXibOne

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self customView];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self customView];
    }
    return self;
}
-(void)customView
{
    [[NSBundle mainBundle] loadNibNamed:@"ComboXibOne" owner:self options:nil];
    [self addSubview:self.subView];
    self.subView.frame= self.bounds;
    [self addSubview:self.subInView];
    self.subInView.frame= self.bounds;
    //    self.frame= self.bounds;
    [self.subView setHidden:YES];
    [self.subInView setHidden:YES];
}
- (IBAction)menuButtPress:(id)sender {
    [self.subView setHidden:NO];
    [self.subInView setHidden:NO];
    [self.delegate didUpdateView];
}

- (IBAction)homeButtPress:(id)sender {
}

- (IBAction)recycleButtPress:(id)sender {
}

- (IBAction)editButtPress:(id)sender {
}

- (IBAction)locateButtPress:(id)sender {
}

- (IBAction)logoutButtPress:(id)sender {
}
@end
