//
//  TpThreeXibTwo.m
//  News Feed
//
//  Created by 4-OM on 12/5/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "TpThreeXibTwo.h"

@implementation TpThreeXibTwo

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self commonSetup];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self commonSetup];
    }
    return self;
}
-(void)customView
{
    [[NSBundle mainBundle] loadNibNamed:@"TpThreeXibTwo" owner:self options:nil];
    [self addSubview:self.hidden];
    self.hidden.frame= self.bounds;
//    [self addSubview:self.subView];
//    self.subView.frame= self.bounds;
//    self.frame= self.bounds;
}
- (UIView *)loadViewFromNib {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    
    //  An exception will be thrown if the xib file with this class name not found,
    UIView *view = [[bundle loadNibNamed:NSStringFromClass([self class])  owner:self options:nil] firstObject];
    return view;
}
- (void)commonSetup {
    UIView *nibView = [self loadViewFromNib];
    nibView.frame = self.bounds;
    // the autoresizingMask will be converted to constraints, the frame will match the parent view frame
    nibView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    // Adding nibView on the top of our view
    [self addSubview:nibView];
}
//-(void)didUpdateView:(UIView *)NewView
//{
//    [self customView];
//}
//- (IBAction)menuButonPress:(id)sender {
//    NSLog(@"Happpyy");
//    [self.delegate didUpdateView:self.hidden];
//}
- (IBAction)homePress:(id)sender {
    self.hidden.hidden = YES;
}
@end
