//
//  TimeCollectViewController.h
//  News Feed
//
//  Created by 4-OM on 11/29/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeNibCollectionViewCell.h"

@interface TimeCollectViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>

@property (strong, nonatomic) IBOutlet UICollectionView *timeCollection;



@end
