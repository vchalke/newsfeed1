//
//  TimeCollectViewController.m
//  News Feed
//
//  Created by 4-OM on 11/29/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "TimeCollectViewController.h"
#import "TimeNibCollectionViewCell.h"

#define JSON_MAIN_URL @"https://newsapi.org/v2/sources?apiKey=326b273af74c4a80913bca7789f2d898"
#define TOP_NEWS_URL @"https://newsapi.org/v2/top-headlines?sources=abc-news&apiKey=326b273af74c4a80913bca7789f2d898"
@interface TimeCollectViewController () <UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSMutableArray *buttonArray;
    NSMutableArray *buttonId;
    NSMutableArray *imgArray;
    NSMutableArray *text1Array;
    NSMutableArray *text2Array;
    NSMutableDictionary *json ;
    NSMutableArray *jsonArray ;
    NSMutableArray *jsonArrayId ;
    NSMutableArray *jsonArrayName ;
    NSString *topNewsApi;
    NSString *api1;
    NSString *api2;
    NSString *api3;
    NSInteger *numTitle;
    NSInteger *numId;
    NSMutableDictionary *json1 ;
    NSMutableArray *jsonArtArr;
    NSMutableArray *jsonTitleArr;
    NSMutableArray *jsonDescArr;
    NSMutableArray *jsonUrlImgArr;
    
}

@end

@implementation TimeCollectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.timeCollection.delegate = self;
    self.timeCollection.dataSource = self;
    [self.timeCollection registerNib:[UINib nibWithNibName:@"TimeNibCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TimeNibCollection"];
    api1 = @"https://newsapi.org/v2/top-headlines?sources=";
    api2 = @"&apiKey=326b273af74c4a80913bca7789f2d898";
    NSError *error;
    NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:JSON_MAIN_URL]];
    json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSLog(@"%@",json);
    NSLog(@"%ld",json.count);
    jsonArray = [json objectForKey:@"sources"];
    NSLog(@"%@",jsonArray);
    buttonArray = [jsonArray valueForKey:@"name"];
    NSLog(@"%@",buttonArray);
    buttonId = [jsonArray valueForKey:@"id"];
    NSLog(@"%@",buttonId);
    
//  buttonArray = [[NSMutableArray alloc]initWithObjects:@"aaaa",@"bbbb",@"cccc",@"dddd",@"eeee",@"ffff",nil];
    imgArray = [[NSMutableArray alloc]initWithObjects:@"ferrari",@"news",@"register",@"login",@"editProfile",@"locate1",nil];
    text1Array = [[NSMutableArray alloc]initWithObjects:@"aaaa",@"bbbb",@"cccc",@"dddd",@"eeee",@"ffff",nil];
    text2Array = [[NSMutableArray alloc]initWithObjects:@"aaaa",@"bbbb",@"cccc",@"dddd",@"eeee",@"ffff",nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section   {
    
    return 0; 
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return buttonArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TimeNibCollectionViewCell *cell;
     cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TimeNibCollection" forIndexPath:indexPath];
    [cell.buttonTimeNib setTitle:[NSString stringWithFormat:@"%@",[buttonArray objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
   numTitle =[buttonArray indexOfObject:cell.buttonTimeNib.titleLabel.text];
//    NSLog(@"ld",numTitle);
    api3 = [buttonId objectAtIndex:numTitle];
//    NSLog(@"%@",api3);
    topNewsApi = [NSString stringWithFormat:@"%@%@%@",api1,api3,api2];
//    NSLog(@"%@",topNewsApi);
    NSError *error;
    NSData *data11 = [NSData dataWithContentsOfURL: [NSURL URLWithString:topNewsApi]];
    json1 = [NSJSONSerialization JSONObjectWithData:data11 options:kNilOptions error:&error];
    jsonArtArr = [json1 objectForKey:@"articles"];
//    NSLog(@"%@",jsonArtArr);
    jsonTitleArr = [jsonArtArr valueForKey:@"title"];
//    NSLog(@"%@",jsonTitleArr);
    jsonDescArr = [jsonArtArr valueForKey:@"description"];
//    NSLog(@"%@",jsonDescArr);
    jsonUrlImgArr = [jsonArtArr valueForKey:@"urlToImage"];
//    NSLog(@"%@",jsonUrlImgArr);
//
//    jsonTable1 = [jsonImg valueForKey:@"title"];
//    NSLog(@"%@",jsonTable1);
//    jsonTable2 = [jsonImg valueForKey:@"description"];
//    NSLog(@"%@",jsonTable2);
    cell.imageTimeNIb.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[jsonUrlImgArr objectAtIndex:1]]];
    cell.text1TimeNib.text = [NSString stringWithFormat:@"%@",[jsonTitleArr objectAtIndex:1]];
    cell.text2TimeNib.text = [NSString stringWithFormat:@"%@",[jsonDescArr objectAtIndex:1]];
    
    return cell;
}
@end
