//
//  TimeNibCollectionViewCell.h
//  News Feed
//
//  Created by 4-OM on 11/29/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeNibCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIButton *buttonTimeNib;
@property (strong, nonatomic) IBOutlet UIImageView *imageTimeNIb;
@property (strong, nonatomic) IBOutlet UITextView *text1TimeNib;
@property (strong, nonatomic) IBOutlet UITextView *text2TimeNib;

@end
