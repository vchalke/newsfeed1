//
//  TimeNibCollectionViewCell.m
//  News Feed
//
//  Created by 4-OM on 11/29/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "TimeNibCollectionViewCell.h"

@implementation TimeNibCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _buttonTimeNib.layer.shadowOffset = CGSizeMake(0.0, 2.0);
    _buttonTimeNib.layer.masksToBounds = false;
    //   _buttonn.layer.shadowRadius = 2.0;
    _buttonTimeNib.layer.shadowOpacity = 0.2;
    
    _buttonTimeNib.layer.borderWidth = 1.5f;
    _buttonTimeNib.layer.borderColor = [UIColor colorWithRed:255 green:240 blue:250 alpha:1.0].CGColor;
}

@end
