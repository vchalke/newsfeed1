//
//  RegData+CoreDataClass.h
//  News Feed
//
//  Created by 4-OM on 11/16/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Image;

NS_ASSUME_NONNULL_BEGIN

@interface RegData : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "RegData+CoreDataProperties.h"
