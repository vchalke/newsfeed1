//
//  ScrolleeViewController.h
//  News Feed
//
//  Created by 4-OM on 11/29/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScrolleeViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scroll1;
@property (strong, nonatomic) IBOutlet UIView *view1;
@property (strong, nonatomic) IBOutlet UIButton *yourButton;

@end
