//
//  EditProfileViewController.m
//  News Feed
//
//  Created by 4-OM on 11/14/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "EditProfileViewController.h"
#import "ChgPassViewController.h"
#define EMAIL_ID @"emailid"
#define MAIL_TEXT @"emailid"
//#import "CoreDataHelper.h"


@interface EditProfileViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    BOOL infoIsValid;
    BOOL updateBOOL;
}

@property (nonatomic,strong)NSMutableArray *images;

@end

@implementation EditProfileViewController
@synthesize updateInfo;

//-(NSMutableArray *)images
//{
//    if (! _images)
//    {
//        _images = (NSMutableArray *)_images;
//    }
//    return _images;
//}

-(NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context=nil;
    _delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if([_delegate respondsToSelector:@selector(persistentContainer)])
    {
        context= _delegate.persistentContainer.viewContext;
    }
    return context;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"image"];
    UIImage* image = [UIImage imageWithData:imageData];
    self.image_view.image = image;


    if (self.updateInfo)
    {
        [self.nametextfield setText:[self.updateInfo valueForKey:@"name"]];
        [self.mailtextfield setText:[self.updateInfo valueForKey:@"emailid"]];
        [self.passtextfield setText:[self.updateInfo valueForKey:@"pass"]];
        [self.confpasstextfield setText:[self.updateInfo valueForKey:@"confpass"]];
        [self.locatetextfield setText:[self.updateInfo valueForKey:@"location"]];
        [self.contacttextfield setText:[self.updateInfo valueForKey:@"contact"]];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"FromEditToChg"])
    {
        
        [self useObject];
        
        for (NSManagedObject * inform  in _editPassInfo){
            NSString *emailid = [NSString stringWithFormat:@"%@",[inform valueForKey:@"emailid"]];
            if ([emailid isEqualToString:self.mailtextfield.text]){
                ChgPassViewController *changePass= segue.destinationViewController;
                changePass.passInfo = inform;
                
            }
        }
    }
}

-(void)useObject
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest=[[NSFetchRequest alloc]initWithEntityName:@"RegData"];
    self.editPassInfo=[[managedObjectContext executeFetchRequest:fetchRequest error:nil]mutableCopy];
}
- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)updatePressed:(id)sender {
    
    NSManagedObjectContext *context = [self managedObjectContext];
   
//    [self fetchData];
//    if (updateBOOL==NO){
    if (self.updateInfo){
        if ((self.nametextfield.text.length!=0)&&
            (self.mailtextfield.text.length!=0)&&
            (self.passtextfield.text.length!=0)&&
            (self.confpasstextfield.text.length!=0)&&
            (self.locatetextfield.text.length!=0)&&
            (self.contacttextfield.text.length==10)&&
            ([self.passtextfield.text isEqualToString:self.confpasstextfield.text])&&
            ([self.mailtextfield.text containsString:@"@gmail.com"])&&
            ([self infoIsValid:self.passtextfield.text]==YES)   )
        {
            [self.updateInfo setValue:self.nametextfield.text forKey:@"name" ];
            [self.updateInfo setValue:self.mailtextfield.text forKey:@"emailid" ];
            [self.updateInfo setValue:self.passtextfield.text forKey:@"pass" ];
            [self.updateInfo setValue:self.confpasstextfield.text forKey:@"confpass" ];
            [self.updateInfo setValue:self.locatetextfield.text forKey:@"location" ];
            [self.updateInfo setValue:self.contacttextfield.text forKey:@"contact" ];
        }
        else [self showAlert];
    }
        else [self showAlert];
//    }
//        else [self alreadyExist];
    
//    if (!self.updateInfo){
//        if ((self.nametextfield.text.length!=0)&&
//            (self.mailtextfield.text.length!=0)&&
//            (self.passtextfield.text.length!=0)&&
//            (self.confpasstextfield.text.length!=0)&&
//            (self.locatetextfield.text.length!=0)&&
//            (self.contacttextfield.text.length==10)&&
//            ([self.passtextfield.text isEqualToString:self.confpasstextfield.text])&&
//            ([self.mailtextfield.text containsString:@"@gmail.com"])&&
//            ([self infoIsValid:self.passtextfield.text]==YES)   )
//        {
//        NSManagedObject *selfInfo = [NSEntityDescription insertNewObjectForEntityForName:@"RegData" inManagedObjectContext:context];
//            
//            [selfInfo setValue:self.nametextfield.text forKey:@"name"];
//            [selfInfo setValue:self.mailtextfield.text forKey:@"emailid"];
//            [selfInfo setValue:self.passtextfield.text forKey:@"pass"];
//            [selfInfo setValue:self.confpasstextfield.text forKey:@"confpass"];
//            [selfInfo setValue:self.locatetextfield.text forKey:@"location"];
//            [selfInfo setValue:self.contacttextfield.text forKey:@"contact"];
//        }
//        else [self showAlert];
//    }
    
    [[NSUserDefaults standardUserDefaults]setObject:self.mailtextfield.text forKey:MAIL_TEXT];
    
    NSError *error = nil;
    if(![context save:&error])
    {
        NSLog(@"Cant save %@ %@",error,[error localizedDescription]);
    }

}

- (IBAction)chgPassPressed:(id)sender {
        [self performSegueWithIdentifier:@"FromEditToChg" sender:nil];
}

- (IBAction)editPhotoPressed:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    [self presentViewController:picker animated:YES completion : nil];
}


#pragma mark UIImagePickerControllerDelegate
///////////////////////////////////////////

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    if (! image){
        image = info[UIImagePickerControllerOriginalImage];
    }
    if (image){
    self.image_view.image = image;
        
        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(image) forKey:@"image"];
    }

//    [self.photos addObject : [self photoFromImage:image]];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    NSLog(@"Cancel");
    [self dismissViewControllerAnimated:YES completion:nil];
}
 
- (BOOL)infoIsValid:(NSString *)password {
    
    // 1. Upper case.
    if (![[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:[password characterAtIndex:0]])
        return NO;
    
    // 2. Length.
    if ([password length] < 6)
        return NO;
    
    // 3. Special characters.
    // Change the specialCharacters string to whatever matches your requirements.
    NSString *specialCharacters = @"!#€%&/()[]=?$§@*'";
    if ([[password componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:specialCharacters]] count] < 2)
        return NO;
    
    // 4. Numbers.
    if ([[password componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]] count] < 2)
        return NO;
    
    return YES;
}
-(void)showAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"7Char,1Num,1Key,1Capital,valid mailid,valid contact,fill all info" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Try Again" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)alreadyExist
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Data already Exist" message:@"Use Another Mail-Id or Contact" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Go back" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                             //                             [self dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
/*
-(void)fetchData
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"RegData" inManagedObjectContext:managedObjectContext];
    NSError *error = nil;
    [fetchRequest setEntity:entity];
    NSArray *fetchedObj = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    //    if(fetchedObj.count > 0){
    
    
    for (NSManagedObjectContext * infor  in fetchedObj)
    {
        NSString *emailid = [NSString stringWithFormat:@"%@",[infor valueForKey:@"emailid"]];
        NSString *contact = [NSString stringWithFormat:@"%@",[infor valueForKey:@"contact"]];
        NSLog(@"%@",emailid);
        NSLog(@"%@",contact);
        if (! [self.mailtextfield.text isEqualToString:emailid] &&
            ! [self.contacttextfield.text isEqualToString:contact])
        {
            updateBOOL=NO;
            NSLog(@"NO");
            
        }
        else {
            updateBOOL=YES;
            NSLog(@"YES");
            break;
        }
        
        //        }
    }
}
*/

/*
 - (void)viewWillAppear:(BOOL)animated {
 [super viewWillAppear:animated];
 
 
 NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
 
 NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
 NSEntityDescription *entity = [NSEntityDescription entityForName:@"RegData" inManagedObjectContext:managedObjectContext];
 NSError *error = nil;
 [fetchRequest setEntity:entity];
 NSArray *fetchObject = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
 
 if(fetchObject.count > 0){
 
 
 for (NSManagedObjectContext * editInfo  in fetchObject)
 {
 NSString *emailid = [NSString stringWithFormat:@"%@",[editInfo valueForKey:@"emailid"]];
 NSString *str=[NSString stringWithFormat:[[NSUserDefaults standardUserDefaults]objectForKey:EMAIL_ID]];
 NSLog(@"%@",emailid);
 NSLog(@"%@",str);
 if ([emailid isEqualToString:str]){
 [self.mailtextfield setText:[NSString stringWithFormat:emailid]];
 [self.nametextfield setText:[NSString stringWithFormat:@"%@",[editInfo valueForKey:@"name"]]];
 [self.passtextfield setText:[NSString stringWithFormat:@"%@",[editInfo valueForKey:@"pass"]]];
 [self.confpasstextfield setText:[NSString stringWithFormat:@"%@",[editInfo valueForKey:@"confpass"]]];
 [self.locatetextfield setText:[NSString stringWithFormat:@"%@",[editInfo valueForKey:@"location"]]];
 [self.contacttextfield setText:[NSString stringWithFormat:@"%@",[editInfo valueForKey:@"contact"]]];
 }
 }
 }
 }
 */
@end
