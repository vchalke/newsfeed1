//
//  MainViewController.m
//  News Feed
//
//  Created by 4-OM on 11/12/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "MainViewController.h"
#import "LoginViewController.h"
#import "EditProfileViewController.h"
//#import "CoreDataHelper.h"

#define EMAIL_ID @"emailid"
#define MAIL_TEXT @"emailid"
//#define PROFILE_IMAGE @"image"

@interface MainViewController ()

@end

@implementation MainViewController
//@synthesize inform;

-(NSMutableArray *) editInfo
{
    if (! _editInfo)
    {
        _editInfo= [[NSMutableArray alloc] init];
    }
    return _editInfo;

}
-(NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context=nil;
    _delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if([_delegate respondsToSelector:@selector(persistentContainer)])
    {
        context= _delegate.persistentContainer.viewContext;
    }
    return context;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tapp = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideSideView:)];
    tapp.numberOfTapsRequired = 1;
    [_sideView addGestureRecognizer:tapp];
    
    self.mailLabel.text=[[NSUserDefaults standardUserDefaults]objectForKey:EMAIL_ID]; 
//    self.mainImageView.image =[[NSUserDefaults standardUserDefaults]objectForKey:PROFILE_IMAGE];
    
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"image"];
    UIImage* image = [UIImage imageWithData:imageData];
    self.mainImageView.image = image;
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"image"];
    UIImage* image = [UIImage imageWithData:imageData];
    self.mainImageView.image = image;
    
    self.mailLabel.text=[[NSUserDefaults standardUserDefaults]objectForKey:MAIL_TEXT];
    NSLog(@"%@",self.mailLabel.text);


}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];


    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest=[[NSFetchRequest alloc]initWithEntityName:@"RegData"];
    self.editInfo=[[managedObjectContext executeFetchRequest:fetchRequest error:nil]mutableCopy];

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"FromMainToEdit"])
    {

        [self viewDidAppear:YES];

        for (NSManagedObject * info  in _editInfo){
            NSString *emailid = [NSString stringWithFormat:@"%@",[info valueForKey:@"emailid"]];
            if ([emailid isEqualToString:self.mailLabel.text]){
        EditProfileViewController *editProfile= segue.destinationViewController;
        editProfile.updateInfo = info;
            
            }
        }
    }
}


- (IBAction)menuPressed:(id)sender {

    
    [_hiddenView setHidden:NO];
    [UIView transitionWithView:_sideView duration:0.2 options:UIViewAnimationOptionCurveEaseIn animations:^{
        CGRect frame = _sideView.frame;
        frame.origin.x=0;
        _sideView.frame = frame;

        
    } completion:nil];


}

- (IBAction)homePressed:(id)sender {
}

- (IBAction)editproPressed:(id)sender {
    [self performSegueWithIdentifier:@"FromMainToEdit" sender:nil];
}

- (IBAction)locationPressed:(id)sender {
    [self performSegueWithIdentifier:@"FromMainToMapp" sender:nil];
}

- (IBAction)logoutPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}



-(void)hideSideView:(UIGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateEnded)  
    {
        [_hiddenView setHidden:YES];
        [UIView transitionWithView:_sideView duration:0.2 options:UIViewAnimationOptionCurveEaseIn animations:^{
            CGRect frame = _sideView.frame;
            frame.origin.x=-_sideView.frame.size.width;
            _sideView.frame = frame;
            
        } completion:nil];
    }
}
@end
