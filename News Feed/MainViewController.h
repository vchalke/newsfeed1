//
//  MainViewController.h
//  News Feed
//
//  Created by 4-OM on 11/12/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "RegData+CoreDataClass.h"
#import "RegData+CoreDataProperties.h"
#import "Image+CoreDataClass.h"
#import "Image+CoreDataProperties.h"
#import "AppDelegate.h"


@interface MainViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *homeButton;
@property (strong, nonatomic) IBOutlet UIButton *editproButton;
@property (strong, nonatomic) IBOutlet UIButton *locationButton;
@property (strong, nonatomic) IBOutlet UIButton *logoutButton;

@property (strong, nonatomic) IBOutlet UILabel *mailLabel;
@property (strong, nonatomic) IBOutlet UIImageView *mainImageView;


@property (strong, nonatomic) IBOutlet UIButton *menuButton;
@property (strong, nonatomic) IBOutlet UIView *hiddenView;
@property (strong, nonatomic) IBOutlet UIView *sideView;

@property (strong , nonatomic)NSManagedObjectContext * managedObjectContext;
//@property (strong , nonatomic)NSManagedObject * inform;
@property (nonatomic, strong) AppDelegate *delegate;
@property (strong , nonatomic)NSMutableArray * editInfo;
- (IBAction)menuPressed:(id)sender;
- (IBAction)homePressed:(id)sender;
- (IBAction)editproPressed:(id)sender;
- (IBAction)locationPressed:(id)sender;
- (IBAction)logoutPressed:(id)sender;


@end
