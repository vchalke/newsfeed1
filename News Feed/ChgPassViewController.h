//
//  ChgPassViewController.h
//  News Feed
//
//  Created by 4-OM on 11/14/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "RegData+CoreDataClass.h"
#import "RegData+CoreDataProperties.h"
#import "Image+CoreDataClass.h"
#import "Image+CoreDataProperties.h"
#import "AppDelegate.h"

@interface ChgPassViewController : UIViewController

@property (strong,nonatomic)NSManagedObject *passInfo;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) AppDelegate *delegate;

@property (strong, nonatomic) IBOutlet UITextField *oldPass;
@property (strong, nonatomic) IBOutlet UITextField *currentPass;
@property (strong, nonatomic) IBOutlet UITextField *confPass;


- (IBAction)backPressed:(id)sender;

@end
