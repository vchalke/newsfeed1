//
//  ConfirmViewController.m
//  News Feed
//
//  Created by 4-OM on 11/10/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "ConfirmViewController.h"

@interface ConfirmViewController ()

@end

@implementation ConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)confirmPressed:(id)sender {
    [self alertView];
}
-(void)alertView
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Enter Your Details Properly" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *Skip = [UIAlertAction actionWithTitle:@"SKIP" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                           {
                           }];
    
//    UIAlertAction *Goback = [UIAlertAction actionWithTitle:@"GO BACK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action)
//                             {
//                                 [self dismissViewControllerAnimated:YES completion:nil];
//                             }];
    
    [alert addAction:Skip];
//    [alert addAction:Goback];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
