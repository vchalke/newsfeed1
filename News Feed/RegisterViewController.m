//
//  RegisterViewController.m
//  News Feed
//
//  Created by 4-OM on 11/10/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "RegisterViewController.h"


@interface RegisterViewController ()
{
    BOOL passwordIsValid;
    BOOL isBOOOL;
}

@end

@implementation RegisterViewController
//@synthesize registerInfo;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    _delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if ([_delegate respondsToSelector:@selector(persistentContainer)])
    {
        context = _delegate.persistentContainer.viewContext;
    }
    return context;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)registerPressed:(id)sender {
    
    NSManagedObjectContext *context = [self managedObjectContext];
    [self fetchData];
    if (isBOOOL==NO){
    
        if ((self.nametext.text.length != 0)&&
            (self.emailidtext.text.length != 0)&&
            ([self.emailidtext.text containsString:[NSString stringWithFormat:@"@gmail.com"]])&&
            (self.passwordtext.text.length != 0)&&
            (self.confpasstext.text.length != 0)&&
            ([self passwordIsValid:self.passwordtext.text]==YES)&& 
            (self.locationtext.text.length != 0)&&
            (self.contacttext.text.length != 0)&&
            ([self.confpasstext.text isEqualToString:self.passwordtext.text])&&
            (self.contacttext.text.length == 10))
        {
        NSManagedObject *newContext = [NSEntityDescription insertNewObjectForEntityForName:@"RegData" inManagedObjectContext:context];
        [newContext setValue:self.nametext.text forKey:@"name"];
        [newContext setValue:self.emailidtext.text forKey:@"emailid"];
        [newContext setValue:self.passwordtext.text forKey:@"pass"];
        [newContext setValue:self.confpasstext.text forKey:@"confpass"];
        [newContext setValue:self.locationtext.text forKey:@"location"];
        [newContext setValue:self.contacttext.text forKey:@"contact"];
        }
        else [self alertView];
        
    }
        else [self alreadyExist];
    
    [self dataSave];
    NSError *error = nil;
    if(![context save:&error])
    {
        NSLog(@"Cant save %@ %@",error,[error localizedDescription]);
    }
    
}

//- (IBAction)backPressed:(id)sender {
//    [self dismissViewControllerAnimated:YES completion:nil];
//}

-(void)alertView
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Enter Your Details Properly" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *Skip = [UIAlertAction actionWithTitle:@"SKIP" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                         {
                         }];

    [alert addAction:Skip];

    [self presentViewController:alert animated:YES completion:nil];
}

-(void)dataSave
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Data Saved" message:@"saved Data Successfully" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Go back" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
//                             [self dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)alreadyExist
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Data already Exist" message:@"Use Another Mail-Id or Contact" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Go back" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
//                             [self dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

- (BOOL)passwordIsValid:(NSString *)password {
    
    // 1. Upper case.
    if (![[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:[password characterAtIndex:0]])
        return NO;
    
    // 2. Length.
    if ([password length] < 6)
        return NO;
    
    // 3. Special characters.
    // Change the specialCharacters string to whatever matches your requirements.
    NSString *specialCharacters = @"!#€%&/()[]=?$§@*'";
    if ([[password componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:specialCharacters]] count] < 2)
        return NO;
    
    // 4. Numbers.
    if ([[password componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]] count] < 2)
        return NO;
    
    return YES;
}


-(void)fetchData
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"RegData" inManagedObjectContext:managedObjectContext];
    NSError *error = nil;
    [fetchRequest setEntity:entity];
    NSArray *fetchedObj = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
//    if(fetchedObj.count > 0){
    
        
        for (NSManagedObjectContext * infor  in fetchedObj)
        {
            NSString *emailid = [NSString stringWithFormat:@"%@",[infor valueForKey:@"emailid"]];
            NSString *contact = [NSString stringWithFormat:@"%@",[infor valueForKey:@"contact"]];
            NSLog(@"%@",emailid);
            NSLog(@"%@",contact);
            if (! [self.emailidtext.text isEqualToString:emailid] &&
                ! [self.contacttext.text isEqualToString:contact])
            {
                isBOOOL=NO;
                NSLog(@"NO");
                
            }
            else {
                isBOOOL=YES;
                 NSLog(@"YES");
                break;
            }
            
//        }
    }
}
@end
