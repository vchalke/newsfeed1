//
//  LoginViewController.m
//  News Feed
//
//  Created by 4-OM on 11/10/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "LoginViewController.h"
#import "MainViewController.h"

#define EMAIL_ID @"emailid"

@interface LoginViewController ()

@end

@implementation LoginViewController

-(NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context=nil;
    _delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if([_delegate respondsToSelector:@selector(persistentContainer)])
    {
        context= _delegate.persistentContainer.viewContext;
    }
    return context;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

}

-(void)viewWillAppear:(BOOL)animated
{
    self.emailid_text.text = @"";
    self.pass_text.text = @"";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"FromLogToMain"])
    {
//    NSLog(@"Happy");
    }
}



- (IBAction)forgotPressed:(id)sender {
    [self performSegueWithIdentifier:@"FromLogToConf" sender:nil];
}

- (IBAction)newUserPressed:(id)sender {
    [self performSegueWithIdentifier:@"FromLoginToReg" sender:nil];
}

- (IBAction)loginPressed:(id)sender {
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"RegData" inManagedObjectContext:managedObjectContext];
    NSError *error = nil;
    [fetchRequest setEntity:entity];
    NSArray *fetchedObj = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
   
    if(fetchedObj.count > 0){
    
    
    for (NSManagedObjectContext * infor  in fetchedObj)
    {
        NSString *emailid = [NSString stringWithFormat:@"%@",[infor valueForKey:@"emailid"]];
        NSString *password = [NSString stringWithFormat:@"%@",[infor valueForKey:@"pass"]];
        NSLog(@"%@",emailid);
        NSLog(@"%@",password);

        
        
        if ((self.emailid_text.text.length != 0)&&
            (self.pass_text.text.length != 0))
        {
            if ([self.emailid_text.text isEqualToString:emailid] &&
                [self.pass_text.text isEqualToString:password])
            {
                [[NSUserDefaults standardUserDefaults]setObject:self.emailid_text.text forKey:EMAIL_ID];
                [self performSegueWithIdentifier:@"FromLogToMain" sender:nil];                
            }
            
        
        }
        
       else if (self.emailid_text.text.length == 0)
            {
                if ((self.emailid_text.text.length == 0)&&
                    (self.pass_text.text.length == 0))
                    {
                        [self blankalertShow];
                    }
                else
                    [self blankMailAlert];
            
            }
        
        else if (self.pass_text.text.length == 0)
            {
                if ((self.emailid_text.text.length == 0)&&
                    (self.pass_text.text.length == 0))
                    {
                        [self blankalertShow];
                    }
                else
                    [self blankPassAlert];
            
            }
        
        else if ((self.emailid_text.text != emailid)&&
                (self.pass_text.text == password))
            {
                [self wrongMailAlert];
            }
        
        else if ((self.emailid_text.text == emailid)&&
                (self.pass_text.text != password))
            {
                [self wrongPassAlert];
            }
        
//            if ((self.emailid_text.text != emailid)&&
//                (self.pass_text.text != password))
//            {
//                NSLog(@"%@ %@",emailid,password);
//                [self wrongalertShow];
//            }
        
        
    }
        
    }

}

- (IBAction)segmentPressed:(id)sender {
    
    if(self.segmentControl.selectedSegmentIndex==1)
    {
       [self performSegueWithIdentifier:@"FromLoginToReg" sender:nil];
    }
}


-(void)blankalertShow
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Incomplete Email-Id or Password" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Enter Again" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)wrongalertShow
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Wrong Email-Id or Password" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Enter Again" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)wrongMailAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Wrong Email-Id" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Enter Proper Email-Id" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)wrongPassAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Wrong Password" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Enter Proper Password" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)blankMailAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Incomplete Email-Id" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Enter Email-Id" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)blankPassAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Incomplete Password" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Enter Password" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

//-(void)loginSuccess
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Done" message:@"Login auccessfully" preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
//                         {
//                         }];
//
//    [alert addAction:ok];
//    [self presentViewController:alert animated:YES completion:nil];
//}
@end
