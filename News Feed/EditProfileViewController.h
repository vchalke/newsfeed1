//
//  EditProfileViewController.h
//  News Feed
//
//  Created by 4-OM on 11/14/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "RegData+CoreDataClass.h"
#import "RegData+CoreDataProperties.h"
#import "Image+CoreDataClass.h"
#import "Image+CoreDataProperties.h"
#import "AppDelegate.h"


@interface EditProfileViewController : UIViewController

@property (strong , nonatomic)NSManagedObject *updateInfo;
@property (nonatomic, strong) NSMutableArray *editPassInfo;
@property (strong , nonatomic)NSManagedObject * info;
@property (nonatomic, strong) UIImagePickerController *imagePicked;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) AppDelegate *delegate;
@property (strong, nonatomic) IBOutlet UIImageView *image_view;
@property (strong, nonatomic) IBOutlet UITextField *nametextfield;
@property (strong, nonatomic) IBOutlet UITextField *mailtextfield;
@property (strong, nonatomic) IBOutlet UITextField *passtextfield;
@property (strong, nonatomic) IBOutlet UITextField *confpasstextfield;
@property (strong, nonatomic) IBOutlet UITextField *locatetextfield;
@property (strong, nonatomic) IBOutlet UITextField *contacttextfield;


- (IBAction)backPressed:(id)sender;
- (IBAction)updatePressed:(id)sender;
- (IBAction)chgPassPressed:(id)sender;
- (IBAction)editPhotoPressed:(id)sender;

@end
