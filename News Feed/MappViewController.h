//
//  MappViewController.h
//  News Feed
//
//  Created by 4-OM on 11/12/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MappViewController : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
}
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *myLocationButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *backButton;

- (IBAction)myLocationPressed:(id)sender;
- (IBAction)backPressed:(id)sender;

@end
