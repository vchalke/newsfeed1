//
//  LoginViewController.h
//  News Feed
//
//  Created by 4-OM on 11/10/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "RegData+CoreDataClass.h"
#import "RegData+CoreDataProperties.h"
#import "Image+CoreDataClass.h"
#import "Image+CoreDataProperties.h"
#import "AppDelegate.h"
#import "RegisterViewController.h"
#import "ConfirmViewController.h"



@interface LoginViewController : UIViewController

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) AppDelegate *delegate;

@property (strong, nonatomic) IBOutlet UITextField *emailid_text;
@property (strong, nonatomic) IBOutlet UITextField *pass_text;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControl;


@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentPressed;

- (IBAction)forgotPressed:(id)sender;

- (IBAction)newUserPressed:(id)sender;

- (IBAction)loginPressed:(id)sender;
- (IBAction)segmentPressed:(id)sender;

@end
