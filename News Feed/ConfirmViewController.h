//
//  ConfirmViewController.h
//  News Feed
//
//  Created by 4-OM on 11/10/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "RegData+CoreDataClass.h"
#import "RegData+CoreDataProperties.h"
#import "Image+CoreDataClass.h"
#import "Image+CoreDataProperties.h"
#import "AppDelegate.h"

@interface ConfirmViewController : UIViewController


- (IBAction)confirmPressed:(id)sender;


@end
