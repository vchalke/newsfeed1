//
//  ChgPassViewController.m
//  News Feed
//
//  Created by 4-OM on 11/14/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "ChgPassViewController.h"

@interface ChgPassViewController ()

@end

@implementation ChgPassViewController
@synthesize passInfo;

-(NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context=nil;
    _delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if([_delegate respondsToSelector:@selector(persistentContainer)])
    {
        context= _delegate.persistentContainer.viewContext;
    }
    return context;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.passInfo)
    {
        
        [self.oldPass setText:[self.passInfo valueForKey:@"pass"]];
        
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
