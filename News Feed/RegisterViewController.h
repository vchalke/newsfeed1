//
//  RegisterViewController.h
//  News Feed
//
//  Created by 4-OM on 11/10/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "RegData+CoreDataClass.h"
#import "RegData+CoreDataProperties.h"
#import "Image+CoreDataClass.h"
#import "Image+CoreDataProperties.h"
#import "AppDelegate.h"

@interface RegisterViewController : UIViewController

//@property (nonatomic, strong) NSManagedObjectContext *registerInfo;
@property (nonatomic, strong) AppDelegate *delegate;

@property (strong, nonatomic) IBOutlet UITextField *nametext;
@property (strong, nonatomic) IBOutlet UITextField *emailidtext;
@property (strong, nonatomic) IBOutlet UITextField *passwordtext;
@property (strong, nonatomic) IBOutlet UITextField *confpasstext;
@property (strong, nonatomic) IBOutlet UITextField *locationtext;
@property (strong, nonatomic) IBOutlet UIButton *registerButton;
@property (strong, nonatomic) IBOutlet UITextField *contacttext;


- (IBAction)registerPressed:(id)sender;


@end
