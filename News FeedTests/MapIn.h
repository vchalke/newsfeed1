//
//  MapIn.h
//  News FeedTests
//
//  Created by 4-OM on 11/14/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapIn : NSObject<MKAnnotation>


{
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *description;
}

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy)NSString *title;
@property (nonatomic,copy)NSString *description; 



@end
