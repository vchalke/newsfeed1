//
//  RegData+CoreDataProperties.m
//  News Feed
//
//  Created by 4-OM on 11/16/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//
//

#import "RegData+CoreDataProperties.h"

@implementation RegData (CoreDataProperties)

+ (NSFetchRequest<RegData *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"RegData"];
}

@dynamic confpass;
@dynamic contact;
@dynamic emailid;
@dynamic image;
@dynamic location;
@dynamic name;
@dynamic pass;
@dynamic photos;

@end
