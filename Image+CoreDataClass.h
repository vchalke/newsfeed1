//
//  Image+CoreDataClass.h
//  News Feed
//
//  Created by 4-OM on 11/16/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject, RegData;

NS_ASSUME_NONNULL_BEGIN

@interface Image : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Image+CoreDataProperties.h"
