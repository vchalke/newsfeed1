//
//  collectTableViewController.m
//  News Feed
//
//  Created by 4-OM on 12/1/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "collectTableViewController.h"
#import "CollectionTableNibViewCell.h"
#define JSON_FILE_URL @"https://newsapi.org/v2/sources?apiKey=326b273af74c4a80913bca7789f2d898"
@interface collectTableViewController ()
{
    NSMutableDictionary *json ;
    NSMutableArray *jsonArray ;
    NSMutableArray *jsonArrayName ;
    NSMutableArray *jsonArrayUrl ;
}
@end

@implementation collectTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
            [self.colllectionButton registerNib:[UINib nibWithNibName:@"CollectionTableNibViewCell" bundle:nil] forCellWithReuseIdentifier:@"CollectionTableNib"];
    NSError *error;
    NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:JSON_FILE_URL]];
    json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    jsonArray = [json objectForKey:@"sources"];
    //    NSLog(@"%@",jsonArray);
    jsonArrayName = [jsonArray valueForKey:@"name"];
    //    NSLog(@"%@",jsonArrayId);
    jsonArrayUrl = [jsonArray valueForKey:@"url"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
     return jsonArrayName.count; 
 
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CollectionTableNibViewCell *cell;
    cell = (CollectionTableNibViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionTableNib" forIndexPath:indexPath];
    [cell.butt setTitle:[NSString stringWithFormat:@"%@", [jsonArrayName objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
    return cell;
    
}
@end
