//
//  CollectionTableNibViewCell.m
//  News Feed
//
//  Created by 4-OM on 12/1/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "CollectionTableNibViewCell.h"

@implementation CollectionTableNibViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
    _butt.layer.shadowOffset = CGSizeMake(0.0, 2.0);
    _butt.layer.masksToBounds = false;
    _butt.layer.shadowOpacity = 0.2;
    
    _butt.layer.borderWidth = 1.5f;
    _butt.layer.borderColor = [UIColor colorWithRed:255 green:240 blue:250 alpha:1.0].CGColor;
}

@end
