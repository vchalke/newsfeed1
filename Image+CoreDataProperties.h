//
//  Image+CoreDataProperties.h
//  News Feed
//
//  Created by 4-OM on 11/16/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//
//

#import "Image+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Image (CoreDataProperties)

+ (NSFetchRequest<Image *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSObject *image;
@property (nullable, nonatomic, retain) RegData *albumStore;

@end

NS_ASSUME_NONNULL_END
