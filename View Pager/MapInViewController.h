//
//  MapInViewController.h
//  News Feed
//
//  Created by 4-OM on 12/5/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "TpThreeXibView1.h"
#import "TpThreeXibTwo.h"
#import "ComboXibOne.h"
@protocol MapInViewControllerDelegate<NSObject>

-(void)didUpdateText:(NSString *)coOrdinate;

@end
@interface MapInViewController : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate,TpThreeXibView1Delegate,TpThreeXibTwoDelegate,ComboXibOneDelegate>
{
    CLLocationManager *locationManager;
    CLLocation *location;
    CLLocationCoordinate2D coordinate;
}
@property (weak,nonatomic) id <MapInViewControllerDelegate>delegate;

@property (strong, nonatomic) IBOutlet MKMapView *mapInView;
@property (strong, nonatomic) IBOutlet UIButton *locateMeButton;


- (IBAction)locateMePress:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UIView *belowView;


@end
