//
//  RootPagerContentViewController.m
//  News Feed
//
//  Created by 4-OM on 12/4/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "RootPagerContentViewController.h"
#import "RootPagerViewController.h"
#import "TpTwoWebViewController.h"

@interface RootPagerContentViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableDictionary *json ;
    NSMutableArray *jsonArticles ;
    NSMutableArray *jsonSources ;
    NSMutableArray *jsonName ;
    NSMutableArray *jsonArtTitle ;
    NSMutableArray *jsonUrlToImg ;
    NSMutableArray *jsonDesc ;
    NSMutableArray *jsonUrl ;
    NSString *urlForWeb;
    NSInteger index;
}

@end

@implementation RootPagerContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callTableApi];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)callTableApi{
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        NSError *error;
        NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:_tableViewApi]];
        json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
        jsonArticles = [json objectForKey:@"articles"];
            jsonSources = [jsonArticles valueForKey:@"source"];
            NSLog(@"%@",jsonSources);
            jsonName = [jsonSources valueForKey:@"name"];
            NSLog(@"%@",jsonName);
            jsonArtTitle = [jsonArticles valueForKey:@"title"];
            NSLog(@"%@",jsonArtTitle);
            jsonUrlToImg = [jsonArticles valueForKey:@"urlToImage"];
            NSLog(@"%@",jsonUrlToImg);
            jsonDesc = [jsonArticles valueForKey:@"description"];
            NSLog(@"%@",jsonDesc);
            jsonUrl = [jsonArticles valueForKey:@"url"];
            NSLog(@"%@",jsonUrl);
            self.titlLabel.text = [NSString stringWithFormat:@"%@",[jsonName objectAtIndex:0]];
            NSLog(@"%@",self.titlLabel.text);
            self.buttonOnImg.titleLabel.numberOfLines = 2;
            [self.buttonOnImg setTitle:[NSString stringWithFormat:@"%@",[jsonArtTitle objectAtIndex:0]] forState:UIControlStateNormal];
            self.rootPageImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[jsonUrlToImg objectAtIndex:0]]]]];
            [self.rootPageTable reloadData];
            
            // Change the size of page view controller
            
        });
    });
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    TpTwoWebViewController *webpage = segue.destinationViewController;
    webpage.webUrl = urlForWeb;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

return jsonArtTitle.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"RootPageCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];

    cell.textLabel.text = [NSString stringWithFormat:@"%@", [jsonArtTitle objectAtIndex:indexPath.row]];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [jsonDesc objectAtIndex:indexPath.row]];
    cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [jsonUrlToImg objectAtIndex:indexPath.row]]]]];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *webTitle = selectedCell.textLabel.text;
    
    index = [jsonArtTitle indexOfObject:webTitle];
    
    urlForWeb = [NSString stringWithFormat:@"%@",[jsonUrl objectAtIndex:index]];
    [self performSegueWithIdentifier:@"FromRootContentToWeb" sender:nil];
}
- (IBAction)imgButtonPress:(id)sender {
    urlForWeb = [NSString stringWithFormat:@"%@",[jsonUrl objectAtIndex:0]];
    [self performSegueWithIdentifier:@"FromRootContentToWeb" sender:nil];
}
@end
