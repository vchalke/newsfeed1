//
//  MapInViewController.m
//  News Feed
//
//  Created by 4-OM on 12/5/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "MapInViewController.h"
#import "ComboXibOne.h"
@interface MapInViewController ()
{
    NSString *latitude;
    NSString *longitude;
    NSString *combo;
}

@end

@implementation MapInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.mapInView.delegate = self;
    locationManager.delegate = self;
    locationManager = [[CLLocationManager alloc]init];
    
        TpThreeXibView1 *tpThreeXibView1 =  [[TpThreeXibView1 alloc]initWithFrame:CGRectMake(0, 0, self.mainView.frame.size.width, 60)];
        [self.mainView addSubview:tpThreeXibView1];
        tpThreeXibView1.delegate = self;
    
    UITapGestureRecognizer *tapp = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideBelowView:)];
    tapp.numberOfTapsRequired = 1;
    [self.belowView addGestureRecognizer:tapp];
     [_belowView setHidden:YES];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}
-(void)dismissKeyboard {
    [self.view endEditing:true];
}
-(void)didUpdate:(NSString *)cateText
{
    [_belowView setHidden:NO];
    TpThreeXibTwo *tpThreeXibtwo = [[TpThreeXibTwo alloc]initWithFrame:CGRectMake(self.belowView.frame.origin.x +200,self.belowView.frame.origin.y +200, 260, 300)];
    [self.belowView addSubview:tpThreeXibtwo];
    tpThreeXibtwo.delegate = self;
    
}
-(void)didUpdateView:(UIView *)hideView
{
    NSLog(@"kuch to huaa");
}
-(void)hideBelowView:(UIGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateEnded)
    {
        [UIView transitionWithView:_belowView duration:0.5 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [_belowView setHidden:YES];
            
            
        } completion:nil];
        
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)locateMePress:(id)sender {
    [locationManager requestWhenInUseAuthorization];
    [locationManager requestAlwaysAuthorization];
    [locationManager startUpdatingLocation];
    
    location = [locationManager location];
    coordinate = [location coordinate];
    latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    NSLog(@"%@",latitude);
    NSLog(@"%@",longitude);
//    combo = [NSString stringWithFormat:@"%@,%@",latitude,longitude];
    [self.delegate didUpdateText:[NSString stringWithFormat:@"%@,%@",latitude,longitude]];
//    NSLog(@"%@",combo);
    self.mapInView.showsUserLocation = YES;
//    NSLog(@"%@",_mapInView.userLocation);
}
@end
