//
//  TpViewNibCollectionViewCell.h
//  News Feed
//
//  Created by 4-OM on 12/4/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TpViewNibCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIButton *nibButton;

@end
