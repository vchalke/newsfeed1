//
//  RootPagerViewController.m
//  News Feed
//
//  Created by 4-OM on 12/3/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "RootPagerViewController.h"
#import "RootPageCollectionViewCell.h"
#import "RootPagerContentViewController.h"
#define JSON_FILE_URL @"https://newsapi.org/v2/sources?apiKey=326b273af74c4a80913bca7789f2d898"

@interface RootPagerViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UIPageViewControllerDataSource>
{
    NSMutableDictionary *json ;
    NSMutableArray *jsonArray ;
    NSMutableArray *jsonArrayName ;
    NSMutableArray *jsonArrayUrl ;
    NSMutableArray *jsonId ;
    NSMutableArray *jsonArticles ;
    NSMutableArray *jsonArtTitle ;
    NSMutableArray *jsonUrlToImg ;
    NSMutableArray *jsonPublishId;
    NSMutableArray *filterDevice;
    BOOL isFilter;
    NSInteger butNumber;
}
@end

@implementation RootPagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self callApi];
    [self.collectionInRoot registerNib:[UINib nibWithNibName:@"RootPageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"RootPageCollectionNib"];
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewControl"];
    self.pageViewController.dataSource = self;
     self.searchInRootPager.delegate = self;
     [self.searchInRootPager setHidden:NO];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}
-(void)dismissKeyboard {
    [self.view endEditing:true];
}
-(void)viewWillAppear:(BOOL)animated{

 [self.searchInRootPager setHidden:NO];
}

-(void)callApi{
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        NSError *error;
        NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:JSON_FILE_URL]];
        json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
        jsonArray  = [json objectForKey:@"sources"];
        jsonArrayName  = [jsonArray valueForKey:@"name"];
        jsonArrayUrl = [jsonArray valueForKey:@"url"];
       jsonId = [jsonArray valueForKey:@"id"];
        NSLog(@"%@",jsonId);
        RootPagerContentViewController *startingView = [self viewControllerAtIndex:0];
        NSArray *viewControllers = @[startingView];
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
            
            self.pageViewController.view.frame = CGRectMake(0,140, self.view.frame.size.width, self.view.frame.size.height );
            
            [self addChildViewController:_pageViewController];
            [self.view addSubview:_pageViewController.view];
            [self.pageViewController didMoveToParentViewController:self];
            [self.collectionInRoot reloadData];
            [self.searchInRootPager setHidden:NO];
            self.collectionInRoot.hidden=NO;
        });
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length==0){
        isFilter = false;
    }
    else{
        isFilter = true;
        filterDevice = [[NSMutableArray alloc]init];
        for (NSString *device in jsonArrayName) {
            NSRange nameRange = [device rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if (nameRange.location!= NSNotFound){
                [filterDevice addObject:device];
                
            }
        }
    }
    [self.collectionInRoot reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


//-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"Selected section>> %ld",indexPath.section);
//    NSLog(@"Selected row of section >> %ld",indexPath.row);
//}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (isFilter){
        return filterDevice.count;
    }
     return jsonArrayName.count;

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
//    NSLog(@"Selected section>> %d",indexPath.section);
//    NSLog(@"Selected row of section >> %d",indexPath.row);
    
    RootPageCollectionViewCell *cell;
    cell = (RootPageCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"RootPageCollectionNib" forIndexPath:indexPath];
    if (isFilter){
        [cell.buttCollect setTitle:[NSString stringWithFormat:@"%@", [filterDevice objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
        [cell.buttCollect addTarget:self action:@selector(afterFilter:) forControlEvents:UIControlEventAllEvents];
    }
    else{
    [cell.buttCollect setTitle:[NSString stringWithFormat:@"%@", [jsonArrayName objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
    [cell.buttCollect setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [cell.buttCollect setTag:indexPath.row];
    NSLog(@"%ld",indexPath.row);
    [cell.buttCollect addTarget:self action:@selector(tapButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
    
}
-(void)tapButton:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;
//    [btn setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    NSLog(@"tag %ld", (long)btn.tag);
     CGFloat width = (CGRectGetWidth(self.collectionInRoot.frame)/3);
    self.underLabelWidth.constant = width;
    self.underLabelXAxis.constant = btn.tag * width;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
    RootPagerContentViewController *startingRootPager = [self viewControllerAtIndex:btn.tag];
    NSArray *viewControllers = @[startingRootPager];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
//    [self viewControllerAtIndex:btn.tag];
}
-(void)afterFilter:(id)sender
{
    UIButton *buttn1 = (UIButton *)sender;
    NSLog(@"The button title is %@",buttn1.titleLabel.text);
    //    NSLog(@"tag %ld", (long)butn1.tag);
    butNumber = [jsonArrayName indexOfObject:buttn1.titleLabel.text];
    NSLog(@"tag %ld", butNumber);
    RootPagerContentViewController *startingRootPager = [self viewControllerAtIndex:butNumber];
    NSArray *viewControllers = @[startingRootPager];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [jsonArrayName count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    
    return 0;
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((RootPagerContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    
    index--;
    NSLog(@"Before %ld",index);
    if (index ==0){
//        [self regPress];
    }
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((RootPagerContentViewController*) viewController).pageIndex;
    //    NSLog(@"After %ld",index);
    if (index == NSNotFound) {
        return nil;
    }
    index++;
    
    NSLog(@"After %ld",index);
    if (index == [jsonArrayName count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}
- (RootPagerContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    //    NSUInteger index = ((ContentViewController*) viewController).pageIndex;
    if (([jsonArrayName count] == 0) || (index >= [jsonArrayName count])) {
        return nil;
    }
    
    RootPagerContentViewController *rootPagerContent = [self.storyboard instantiateViewControllerWithIdentifier:@"rootPagerContent"];
    rootPagerContent.titleText = jsonArrayName[index];
    NSLog(@"%@",rootPagerContent.titleText);
    rootPagerContent.pageIndex = index;
    NSLog(@"%ld",rootPagerContent.pageIndex);

    NSInteger numTitle =[jsonArrayName indexOfObject:rootPagerContent.titleText];
        NSLog(@"%ld",numTitle);
    NSString *api3 = [jsonId objectAtIndex:numTitle];
    NSString *topNewsApi = [NSString stringWithFormat:@"https://newsapi.org/v2/top-headlines?sources=%@&apiKey=326b273af74c4a80913bca7789f2d898",api3];
    NSLog(@"%@",topNewsApi);
    rootPagerContent.tableViewApi = topNewsApi;

    return rootPagerContent;
    
}
- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
