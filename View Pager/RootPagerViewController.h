//
//  RootPagerViewController.h
//  News Feed
//
//  Created by 4-OM on 12/3/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootPageCollectionViewCell.h"
#import "RootPagerContentViewController.h"

@interface RootPagerViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UIPageViewControllerDataSource,UISearchBarDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionInRoot;
@property (strong, nonatomic) IBOutlet UILabel *underLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *underLabelWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *underLabelXAxis;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) IBOutlet UISearchBar *searchInRootPager;

- (IBAction)backPressed:(id)sender;

@end
