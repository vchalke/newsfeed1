//
//  RootPageCollectionViewCell.m
//  News Feed
//
//  Created by 4-OM on 12/3/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "RootPageCollectionViewCell.h"

@implementation RootPageCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.buttCollect.titleLabel.minimumScaleFactor = 0.5f;
    self.buttCollect.titleLabel.adjustsFontSizeToFitWidth = YES;
}

@end
