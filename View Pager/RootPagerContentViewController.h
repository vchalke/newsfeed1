//
//  RootPagerContentViewController.h
//  News Feed
//
//  Created by 4-OM on 12/4/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootPagerViewController.h"


@interface RootPagerContentViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imgUrlText;
@property NSString *buttonText;
@property NSString *publishText;
@property NSString *tableViewApi;

@property (strong, nonatomic) IBOutlet UILabel *titlLabel;
@property (strong, nonatomic) IBOutlet UILabel *publishLabel;

@property (strong, nonatomic) IBOutlet UIImageView *rootPageImage;
@property (strong, nonatomic) IBOutlet UITableView *rootPageTable;
@property (strong, nonatomic) IBOutlet UIButton *buttonOnImg;

- (IBAction)imgButtonPress:(id)sender;

@end
