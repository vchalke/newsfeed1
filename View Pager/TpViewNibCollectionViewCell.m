//
//  TpViewNibCollectionViewCell.m
//  News Feed
//
//  Created by 4-OM on 12/4/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "TpViewNibCollectionViewCell.h"

@implementation TpViewNibCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.nibButton.titleLabel.minimumScaleFactor = 0.5f;
    self.nibButton.titleLabel.adjustsFontSizeToFitWidth = YES;
}

@end
