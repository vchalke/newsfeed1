//
//  RegData+CoreDataProperties.h
//  News Feed
//
//  Created by 4-OM on 11/16/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//
//

#import "RegData+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface RegData (CoreDataProperties)

+ (NSFetchRequest<RegData *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *confpass;
@property (nullable, nonatomic, copy) NSString *contact;
@property (nullable, nonatomic, copy) NSString *emailid;
@property (nullable, nonatomic, retain) NSData *image;
@property (nullable, nonatomic, copy) NSString *location;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *pass;
@property (nullable, nonatomic, retain) Image *photos;

@end

NS_ASSUME_NONNULL_END
