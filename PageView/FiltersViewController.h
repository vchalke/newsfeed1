//
//  FiltersViewController.h
//  News Feed
//
//  Created by 4-OM on 11/28/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol FiltersViewControllerDelegate<NSObject>

-(void)didUpdateText:(NSString *)cateText;
-(void)didUpdateTextLang:(NSString *)langText;
-(void)didUpdateTextCount:(NSString *)countText;

@end

@interface FiltersViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak,nonatomic) id <FiltersViewControllerDelegate>delegate;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) IBOutlet UIButton *clearButton;
@property (strong, nonatomic) IBOutlet UIButton *categoryButton;
@property (strong, nonatomic) IBOutlet UIButton *langButton;
@property (strong, nonatomic) IBOutlet UIButton *countryButton;
@property (strong, nonatomic) IBOutlet UITableView *tableViewOne;
@property (strong, nonatomic) IBOutlet UITableView *tableViewTwo;
@property (strong, nonatomic) IBOutlet UITableView *tableViewThree;
@property (strong, nonatomic) IBOutlet UIButton *applyButton;
@property (strong, nonatomic) IBOutlet UIView *hiddenVieww;
@property (strong, nonatomic) IBOutlet UITextField *categoryText;

@property (strong, nonatomic) IBOutlet UITextField *languageText;
@property (strong, nonatomic) IBOutlet UITextField *countryText;
@property (strong, nonatomic) IBOutlet UIScrollView *scrolInFilter;
@property (strong, nonatomic) IBOutlet UIView *viewInFilter;

@property (strong, nonatomic)NSString *catetext;
@property (strong, nonatomic)NSString *langtext;
@property (strong, nonatomic)NSString *countext;


- (IBAction)clearPress:(id)sender;
- (IBAction)categPress:(id)sender;
- (IBAction)countryPress:(id)sender;

- (IBAction)langPress:(id)sender;
- (IBAction)applyPress:(id)sender;


@end
