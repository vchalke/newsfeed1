//
//  CollectionViewController.h
//  News Feed
//
//  Created by 4-OM on 11/17/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewController : UIViewController<UICollectionViewDelegate>

@end

