//
//  FiltersViewController.m
//  News Feed
//
//  Created by 4-OM on 11/28/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "FiltersViewController.h"
#define JSON_MAIN_URL @"https://newsapi.org/v2/sources?apiKey=326b273af74c4a80913bca7789f2d898"
//#define CATEGORY_TEXT @"categtext"
//#define LANG_TEXT @"langtext"
//#define COUNTRY_TEXT @"countrytext"

@interface FiltersViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableDictionary *json ;
    NSMutableArray *jsonArray ;
    NSMutableArray *jsonCategory;
    NSMutableArray *jsonLanguage ;
    NSMutableArray *jsonCountry ;
    
    NSString *cellOneText;
    NSString *cellTwoText;
    NSString *cellThreeText;
    int series;
    

}

@end

@implementation FiltersViewController
@synthesize catetext,langtext,countext;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.scrolInFilter setContentSize:CGSizeMake(414, 850)];
    self.tableViewOne.hidden = YES;
    self.tableViewTwo.hidden = YES;
    self.tableViewThree.hidden = YES;
    [self.categoryText addTarget:self action:@selector(categoryPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.countryText addTarget:self action:@selector(coutryPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.languageText addTarget:self action:@selector(languagePressed) forControlEvents:UIControlEventTouchUpInside];
    [self callApiInFilter];
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
//                                                                          action:@selector(dismissKeyboard)];
//
//    [self.view addGestureRecognizer:tap];

}
//-(void)dismissKeyboard {
//    [self.view endEditing:true];
//}
-(void)callApiInFilter{
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        NSError *error;
        NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:JSON_MAIN_URL]];
        json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            jsonArray = [json objectForKey:@"sources"];
            NSLog(@"%@",jsonArray);
            
            jsonCategory = [jsonArray valueForKey:@"category"];
            //    NSLog(@"%@",jsonCategory);
            NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:jsonCategory];
            jsonCategory = [[NSMutableArray alloc] initWithArray:[mySet array]];
            NSLog(@"%@",jsonCategory);
            jsonLanguage = [jsonArray valueForKey:@"language"];
            //    NSLog(@"%@",jsonLanguage);
            NSOrderedSet *mySet1 = [[NSOrderedSet alloc] initWithArray:jsonLanguage];
            jsonLanguage = [[NSMutableArray alloc] initWithArray:[mySet1 array]];
            NSLog(@"%@",jsonLanguage);
            jsonCountry = [jsonArray valueForKey:@"country"];
            //    NSLog(@"%@",jsonCountry);
            NSOrderedSet *mySet2 = [[NSOrderedSet alloc] initWithArray:jsonCountry];
            jsonCountry = [[NSMutableArray alloc] initWithArray:[mySet2 array]];
            NSLog(@"%@",jsonCountry);
            [self.tableViewOne reloadData];
            [self.tableViewTwo reloadData];
            [self.tableViewThree reloadData];
        });
    });
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableViewOne reloadData];
    [self.tableViewTwo reloadData];
    [self.tableViewThree reloadData];
    if (catetext.length != 0 && langtext.length != 0 && countext.length != 0)
    {
        self.categoryText.text = [NSString stringWithFormat:@"%@",catetext];
        self.languageText.text = [NSString stringWithFormat:@"%@",langtext];
        self.countryText.text = [NSString stringWithFormat:@"%@",countext];
    }
    else{
    self.categoryText.text = nil;
    self.languageText.text = nil;
    self.countryText.text = nil;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)hideView
{

    self.tableViewOne.hidden = YES;
    self.tableViewTwo.hidden = YES;
    self.tableViewThree.hidden = YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backPress:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)clearPress:(id)sender {
    self.categoryText.text = nil;
    self.languageText.text = nil;
    self.countryText.text = nil;
   
}

- (IBAction)categPress:(id)sender {
    [self categoryPressed];
}

- (IBAction)countryPress:(id)sender {
    [self coutryPressed];
}

- (IBAction)langPress:(id)sender {
    [self languagePressed];
}
-(void)categoryPressed
{
    self.tableViewOne.hidden = NO;
    self.tableViewTwo.hidden = YES;
    self.tableViewThree.hidden = YES;
}
-(void)coutryPressed
{
    self.tableViewOne.hidden = YES;
    self.tableViewTwo.hidden = YES;
    self.tableViewThree.hidden = NO;
}
-(void)languagePressed
{
    self.tableViewOne.hidden = YES;
    self.tableViewTwo.hidden = NO;
    self.tableViewThree.hidden = YES;
}
- (IBAction)applyPress:(id)sender {
     
    [self.delegate didUpdateText:self.categoryText.text];
    NSLog(@"%@",self.categoryText.text);
    [self.delegate didUpdateTextLang:self.languageText.text];
    NSLog(@"%@",self.languageText.text);
    [self.delegate didUpdateTextCount:self.countryText.text];
    NSLog(@"%@",self.countryText.text);
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _tableViewOne){
        return jsonCategory.count;
    }
    if (tableView == _tableViewTwo){
                return jsonLanguage.count;
    }
    if (tableView == _tableViewThree){
                return jsonCountry.count;
    }
    return jsonArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifier = @"Tcellone";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (tableView == _tableViewOne){
        static NSString *cellIdentifier = @"Tcellone";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [jsonCategory objectAtIndex:indexPath.row]];
        return cell;
    }
    if (tableView == _tableViewTwo){
        static NSString *cellIdentifier = @"Tcelltwo";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [jsonLanguage objectAtIndex:indexPath.row]];
        return cell;
    }
    if (tableView == _tableViewThree){
        static NSString *cellIdentifier = @"Tcellthree";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [jsonCountry objectAtIndex:indexPath.row]];
        return cell;
    }

    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//        NSLog(@"Selected section>> %d",indexPath.section);
//        NSLog(@"Selected row of section >> %d",indexPath.row);
//    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
//    NSString *cellText = selectedCell.textLabel.text;
//        NSLog(@"%@",cellText);
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tableViewOne){
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        cellOneText = selectedCell.textLabel.text;
        NSLog(@"%@",cellOneText);
        self.categoryText.text = [NSString stringWithFormat:@"%@",cellOneText] ; 

        [self hideView];
    }
    if (tableView == _tableViewTwo){
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        cellTwoText = selectedCell.textLabel.text;
        NSLog(@"%@",cellTwoText);
        self.languageText.text = [NSString stringWithFormat:@"%@",cellTwoText];

        [self hideView];
    }
    if (tableView == _tableViewThree){
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        cellThreeText = selectedCell.textLabel.text;
        NSLog(@"%@",cellThreeText);
        self.countryText.text = [NSString stringWithFormat:@"%@",cellThreeText];

        [self hideView];
    }
    
}
@end
