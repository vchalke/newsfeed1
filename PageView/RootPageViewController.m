//
//  RootPageViewController.m
//  News Feed
//
//  Created by 4-OM on 11/17/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

/*
 
#import "RootPageViewController.h"

@interface RootPageViewController ()

@end

@implementation RootPageViewController


 - (void)viewDidLoad
 {
 [super viewDidLoad];
 
 //    self.dataSource = self;
 
 [self setViewControllers:@[[self.storyboard instantiateViewControllerWithIdentifier:@"one"]] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil]; 
 }
 
 - (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
 {
 if ([viewController isKindOfClass:[PageOneViewController class]])
 return nil;
 
 return [self.storyboard instantiateViewControllerWithIdentifier:@"one"];
 }
 
 - (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
 {
 if ([viewController isKindOfClass:[PageTwoViewController class]])
 return nil;
 
 return [self.storyboard instantiateViewControllerWithIdentifier:@"two"];
 }


/*
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageViewController.dataSource = self; 
    
    _p1 = [[UIViewController alloc] init];
    _p2 = [[UIViewController alloc] init];
    _p3 = [[UIViewController alloc] init];
    
    NSUInteger index = 0;
    NSMutableArray *viewControls = [NSMutableArray arrayWithObjects:[self showVCWithIndex:index], nil];
    
    [self.pageViewController setViewControllers:viewControls
                                      direction:UIPageViewControllerNavigationDirectionForward
                                       animated:NO
                                     completion:nil];
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    
    CGRect pageViewRect = self.view.bounds;
    self.pageViewController.view.frame = pageViewRect;
    
    [self.pageViewController didMoveToParentViewController:self];
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
      viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [self.viewArray indexOfObject:viewController];
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self showVCWithIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
       viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [self.viewArray indexOfObject:viewController];
    
    if (index == 2 || index == NSNotFound) {
        return nil;
    }
    
    index++;
    return [self showVCWithIndex:index];
}

- (UIViewController *)showVCWithIndex: (NSUInteger)index
{
    self.viewArray = [NSArray arrayWithObjects: _p1, _p2, _p3, nil];
    
    UIViewController *currentVC = [self.viewArray objectAtIndex:index];
    
    return currentVC;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end

*/

