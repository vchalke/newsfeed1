//
//  TpTwoNibTableViewCell.h
//  News Feed
//
//  Created by 4-OM on 12/1/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TpTwoNibTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imagOnTable;
@property (strong, nonatomic) IBOutlet UITextView *textViewTitle;
@property (strong, nonatomic) IBOutlet UITextView *textViewDesc;
@property (strong, nonatomic) IBOutlet UITextView *textViewTime;

@end
