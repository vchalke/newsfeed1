//
//  TpOneNibCollectionViewCell.h
//  News Feed
//
//  Created by 4-OM on 11/26/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TpOneNibCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIButton *buttonName;


@end
