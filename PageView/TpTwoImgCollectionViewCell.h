//
//  TpTwoImgCollectionViewCell.h
//  News Feed
//
//  Created by 4-OM on 11/23/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TpTwoImgCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imagee;

@end
