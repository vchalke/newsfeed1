//
//  RootPgViewController.h
//  News Feed
//
//  Created by 4-OM on 11/19/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PageOneViewController.h"
#import "PageTwoViewController.h"
#import "PageThreeViewController.h"

@interface RootPgViewController : UIViewController<UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageViewController;

@property (strong, nonatomic) NSMutableArray *viewControls;
@property (strong, nonatomic) NSArray *viewArray;

@property (strong, nonatomic) PageOneViewController *p1;
@property (strong, nonatomic) PageTwoViewController *p2;
@property (strong, nonatomic) PageThreeViewController *p3;

@end
