//
//  TpTwoViewController.m
//  News Feed
//
//  Created by 4-OM on 11/22/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "TpTwoViewController.h"
#import "TpTwoImgCollectionViewCell.h"
#import "TpTwoWebViewController.h"
#import "TpTwoNibTableViewCell.h"

//#define JSON_FILE_URL_IMG @"https://newsapi.org/v2/top-headlines?sources=abc-news&apiKey=326b273af74c4a80913bca7789f2d898"

//#define JSON_FILE_URL_IMG @"https://newsapi.org/s/abc-news-api"
@interface TpTwoViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource>

{
    NSMutableArray *mutableTp ;
    NSMutableArray *jsonUrlImgTp ;
    NSMutableDictionary *json1 ;
    NSMutableArray *jsonImg ;
    NSMutableArray *jsonUrl ;
    NSMutableArray *jsonUrlImg ;
    NSMutableArray *jsonTable1 ;
    NSMutableArray *jsonTable2 ;
    NSMutableArray *jsonAuthor ;
    NSMutableArray *jsonTable11 ;
    NSMutableArray *jsonTable22 ;
    
    NSString *mainStr;
//    NSString *str2;
    NSString *urll;
    
    NSMutableArray *filterDevice;
    BOOL isFilter;
    NSInteger numdetail;
    int *numimg;
    NSInteger index;
    NSMutableArray *devices;
    
}
@end

@implementation TpTwoViewController
@synthesize buttonTextt;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    isFilter = false;
    self.searchBarr.delegate = self;
    NSLog(@"%@",buttonTextt);
    
// https://newsapi.org/v2/top-headlines?sources=abc-news&apiKey=326b273af74c4a80913bca7789f2d898
//    str1 = @"https://newsapi.org/v2/top-headlines?sources=";
//    str2 = @"&apiKey=326b273af74c4a80913bca7789f2d898";
//    NSString *mainStr = [NSString stringWithFormat: @"%@%@%@", str1,buttonTextt,str2];
    mainStr = [NSString stringWithFormat: @"https://newsapi.org/v2/top-headlines?sources=%@&apiKey=326b273af74c4a80913bca7789f2d898", buttonTextt];
    [self callTpTwoApi];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.viewInTpTwo addGestureRecognizer:tap];
//    jsonUrlImgTp = [[NSMutableArray alloc]initWithObjects:@"ferrari",@"news",@"register",@"login",@"editProfile",@"locate1",nil];

//        NSError *error;
//        NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:mainStr]];
//        json1 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
//
//        jsonImg = [json1 objectForKey:@"articles"];
//        NSLog(@"%@",jsonImg);
//        jsonUrl = [jsonImg valueForKey:@"url"];
//        NSLog(@"%@",jsonUrl);
//        jsonUrlImg = [jsonImg valueForKey:@"urlToImage"];
//        NSLog(@"%@",jsonUrlImg);
//
//        jsonTable1 = [jsonImg valueForKey:@"title"];
//        NSLog(@"%@",jsonTable1);
//        jsonTable2 = [jsonImg valueForKey:@"description"];
//        NSLog(@"%@",jsonTable2);
//
//    self.imae_view11.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [jsonUrlImg objectAtIndex:0]]]]];
//
//    [self.buttonOnImg setTitle:[NSString stringWithFormat:@"%@",[jsonTable1 objectAtIndex:0]] forState:UIControlStateNormal];
//     _buttonOnImg.titleLabel.numberOfLines = 2;
//    [self.view addSubview:_buttonOnImg];
    
}
-(void)dismissKeyboard {
    [self.viewInTpTwo endEditing:true];
}
-(void)callTpTwoApi{
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        NSError *error;
        NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@",mainStr]]];
        json1 = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            jsonImg = [json1 objectForKey:@"articles"];
            NSLog(@"%@",jsonImg);
            jsonUrl = [jsonImg valueForKey:@"url"];
            NSLog(@"%@",jsonUrl);
            jsonUrlImg = [jsonImg valueForKey:@"urlToImage"];
            NSLog(@"%@",jsonUrlImg);
            
            jsonTable1 = [jsonImg valueForKey:@"title"];
            NSLog(@"%@",jsonTable1);
            jsonTable2 = [jsonImg valueForKey:@"description"];
            NSLog(@"%@",jsonTable2);
            
            self.imae_view11.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [jsonUrlImg objectAtIndex:0]]]]];
            
            [self.buttonOnImg setTitle:[NSString stringWithFormat:@"%@",[jsonTable1 objectAtIndex:0]] forState:UIControlStateNormal];
            _buttonOnImg.titleLabel.numberOfLines = 2;
            [self.view addSubview:_buttonOnImg];
            [self.table_View reloadData];
            
        });
    });
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length==0){
        isFilter = false;
    }
    else{
         isFilter = true;
        filterDevice = [[NSMutableArray alloc]init];
        for (NSString *device in jsonTable1) {
            NSRange nameRange = [device rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if (nameRange.location!= NSNotFound){
               
                NSLog(@"Device : %@",device);
                 [filterDevice addObject:device];
                NSLog(@"FilterDevice : %@",filterDevice);

            }
        }
    }
    [self.table_View reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"fromTpTwoToTpWeb"])
    {
        TpTwoWebViewController *webControl= segue.destinationViewController;
        webControl.webUrl = urll;
        NSLog(@"%@",webControl.webUrl);
    }
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isFilter){
        return filterDevice.count;
    }
    else return jsonTable1.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifier = @"TableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (isFilter){
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [filterDevice objectAtIndex:indexPath.row]];
                numdetail = [jsonTable1 indexOfObject: cell.textLabel.text];
                   NSLog(@"%ld",numdetail);
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [jsonTable2 objectAtIndex:numdetail]];
        cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [jsonUrlImg objectAtIndex:numdetail]]]]];

    }
    else{
    cell.textLabel.text = [NSString stringWithFormat:@"%@", [jsonTable1 objectAtIndex:indexPath.row]];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [jsonTable2 objectAtIndex:indexPath.row]];
    cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [jsonUrlImg objectAtIndex:indexPath.row]]]]];
    }
    UIView* shadowView = [[UIView alloc]init];
    [cell setBackgroundView:shadowView];

    shadowView.layer.shadowOffset = CGSizeMake(0.0, 2.0);
    shadowView.layer.masksToBounds = false;
    shadowView.layer.shadowOpacity = 0.2;
    shadowView.layer.borderWidth = 1.5f;
    shadowView.layer.borderColor = [UIColor colorWithRed:255 green:240 blue:250 alpha:1.0].CGColor;
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}
/*
-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableCell"];
//    UITableView* table = (UITableView *)[cell superview];
//    NSIndexPath* pathOfTheCell = [table indexPathForCell:cell];
//    NSInteger rowOfTheCell = [pathOfTheCell row];
//    NSLog(@"rowofthecell %d", rowOfTheCell);
    NSIndexPath *path = [self.table_View indexPathForSelectedRow];
    NSInteger rowOfTheCell = [path row];
    NSLog(@"rowofthecell %ld", rowOfTheCell);
    [self performSegueWithIdentifier:@"fromTpTwoToTpWeb" sender:nil];

}
*/
//fromTpTwoToTpWeb
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"Selected section>> %d",indexPath.section);
//    NSLog(@"Selected row of section >> %d",indexPath.row);
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *cellText = selectedCell.textLabel.text;

    index = [jsonTable1 indexOfObject:cellText];

    urll = [NSString stringWithFormat:@"%@",[jsonUrl objectAtIndex:index]];

     [self performSegueWithIdentifier:@"fromTpTwoToTpWeb" sender:nil];
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    NSLog(@"%ld",jsonUrlImgTp.count);
    return jsonUrlImgTp.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    TpTwoImgCollectionViewCell *cell;
    cell = (TpTwoImgCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"TpTwoImgCollection" forIndexPath:indexPath];

        [cell.imagee setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[jsonUrlImgTp objectAtIndex:indexPath.row]]]];
    
    return cell;
    
}
- (IBAction)backPress:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)butOnImgPress:(id)sender {
     urll = [NSString stringWithFormat:@"%@",[jsonUrl objectAtIndex:0]];
    [self performSegueWithIdentifier:@"fromTpTwoToTpWeb" sender:nil];
}
@end
