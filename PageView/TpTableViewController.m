//
//  TpTableViewController.m
//  News Feed
//
//  Created by 4-OM on 11/20/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "TpTableViewController.h"
//#define JSON_FILE_URL @"https://raw.githubusercontent.com/Binpress/learn-objective-c-in-24-Days/master/Working%20With%20Web%20Data/JSONRead.json"
#define JSON_FILE_URL @"https://newsapi.org/v2/sources?apiKey=326b273af74c4a80913bca7789f2d898"
//#define JSON_FILE_URL @"https://newsapi.org/v2/top-headlines?sources=abc-news&apiKey=326b273af74c4a80913bca7789f2d898"
@interface TpTableViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableDictionary *json ;
    NSMutableArray *jsonArray ;
    NSMutableArray *jsonArrayId ;
    NSMutableArray *jsonArrayUrl ;
//    NSMutableDictionary *items;
    NSMutableArray *namess ;
     NSDictionary *items;
}

@end

@implementation TpTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.title = @"JSONRead";
 
/*
     
    // Download JSON
    NSData *JSONData = [NSData dataWithContentsOfURL:[NSURL URLWithString:JSON_FILE_URL]];
    // Parse JSON
    NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:JSONData options:kNilOptions error:nil];
    NSLog(@"%@",jsonResult);
    
    NSArray *dict = [jsonResult objectForKey:@"sources"];

    NSLog(@"%@",[dict firstObject]);
//    self.data = jsonResult;
    NSMutableArray *_names = [NSMutableArray array];
    for (id item in jsonResult)
//        [_names addObject:[NSString stringWithFormat:@"%@ %@", item[@"fname"], item[@"lname"]]];
        [_names addObject:[NSString stringWithFormat:@"%@", item[@"id"]]];
//        [_names addObject:[NSString stringWithFormat:@"%@", item[@"author"]]];
    self.names = _names;
    NSLog(@"%@",_names);
 
 */

    NSError *error;
    NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:JSON_FILE_URL]];
    json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    jsonArray = [json objectForKey:@"sources"];
//    NSLog(@"%@",jsonArray);
    jsonArrayId = [jsonArray valueForKey:@"id"];
//    NSLog(@"%@",jsonArrayId);
    jsonArrayUrl = [jsonArray valueForKey:@"url"];


//    items = [jsonArray mutableCopy];
//    NSString *jsonStr = [NSString stringWithFormat:[items objectForKey:@"id"]];
//    NSLog(@"%@",jsonStr);
    
//    for (items in jsonArray){
        NSLog(@"%@",[items objectForKey:@"id"]);
//        namess = items[@"id"];
//        NSLog(@"%@",namess);
//    }


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
//    return [self.names count];
    
    NSLog(@"%ld",jsonArrayId.count);
    return jsonArrayId.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//    for (items in jsonArray){
//        NSLog(@"%@",[items objectForKey:@"id"]);
//        namess = items[@"id"];
//        NSLog(@"%@",namess);
//    }

    cell.textLabel.text = [NSString stringWithFormat:@"%@", [jsonArrayId objectAtIndex:indexPath.row]];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [jsonArrayUrl objectAtIndex:indexPath.row]];
//    NSLog(@"%@",cell.textLabel.text);
    return cell;
}
@end
