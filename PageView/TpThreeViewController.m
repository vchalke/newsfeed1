//
//  TpThreeViewController.m
//  News Feed
//
//  Created by 4-OM on 11/23/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "TpThreeViewController.h"
#import "TpThreeCollectionViewCell.h"
#import "TpTwoViewController.h"
#import "FiltersViewController.h"
#import "TpThreeXibView1.h"
#import "TpThreeXibTwo.h"

#define JSON_MAIN_URL @"https://newsapi.org/v2/sources?apiKey=326b273af74c4a80913bca7789f2d898"
#define JSON_TABLE_URL @"https://newsapi.org/v2/top-headlines?sources=abc-news&apiKey=326b273af74c4a80913bca7789f2d898"
//#define CATEGORY_TEXT @"categtext"
//#define LANG_TEXT @"langtext"
//#define COUNTRY_TEXT @"countrytext"

@interface TpThreeViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,FiltersViewControllerDelegate>
{
    NSMutableDictionary *json ;
    NSMutableArray *jsonArray ;
    NSMutableArray *jsonArrayId ;
    NSMutableArray *jsonArrayName ;

    NSMutableDictionary *json1 ;
    NSMutableArray *jsonArt ;
    NSMutableArray *jsonUrlImg ;
    NSMutableArray *jsonTable11;
    NSMutableArray *jsonTable22;
    NSMutableArray *jsonAuthor;
    NSMutableArray *buttonArray ;
    
    NSMutableArray *filterDevice;
    BOOL isFilter;
    int numdetail;
    int index;
    NSInteger buttonNum;
    NSMutableArray *devices;
    int *dictIndex;
    
    NSString *category;
    NSString *language;
    NSString *country;
    NSMutableArray *jsonCategory;
    NSMutableArray *jsonLanguage ;
    NSMutableArray *jsonCountry ;
    NSDictionary *dictCategory;
    NSDictionary *dictCategory1;
    NSDictionary *dictLanguage ;
    NSDictionary *dictCountry ;
//    NSString *str1;
    NSString *str2;
    NSString *str3;
}
@property (strong, nonatomic) IBOutlet TpThreeXibView1 *viewheader;
@end

@implementation TpThreeViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    isFilter = false;
    _viewheader.delegate =self;
    self.search_Bar.delegate = self;
    [self.buttonCollection registerNib:[UINib nibWithNibName:@"TpThreeCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TpThreeCollection"];
    [self callTpThreeApi];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.test1 addGestureRecognizer:tap];
//    TpThreeXibView1 *tpThreeXibView1 = [[TpThreeXibView1 alloc]initWithFrame:CGRectMake(0, 0, self.test1.frame.size.width, 60)];
//    [self.test1 addSubview:tpThreeXibView1];
//    tpThreeXibView1.delegate = self;
    self.hideInTpThr.hidden=YES;
    
//    self.hideInTpThr.hidden=NO;
//    TpThreeXibTwo *tpThreeXibtwo = [[TpThreeXibTwo alloc]initWithFrame:CGRectMake(self.hideInTpThr.frame.origin.x +200,self.hideInTpThr.frame.origin.y +200, 260, 300)];
//    [self.hideInTpThr addSubview:tpThreeXibtwo];
//    tpThreeXibtwo.delegate = self;
//    UITapGestureRecognizer *tapp = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideSideView:)];
//    tapp.numberOfTapsRequired = 1;
//    [self.hideInTpThr addGestureRecognizer:tapp];
    
}
-(void)dismissKeyboard {
    [self.test1 endEditing:true];
}
-(void)didUpdate:(NSString *)cateText{
    str3 = cateText;
    NSLog(@"%@",str3);
//    self.search_Bar.hidden = YES;
//        self.buttonCollection.hidden= YES;
        self.hideInTpThr.hidden=NO;
//    _hideInTpThr.frame = CGRectMake(0, 100, self.view.frame.size.width,  self.view.frame.size.height);
    TpThreeXibTwo *tpThreeXibtwo = [[TpThreeXibTwo alloc]initWithFrame:CGRectMake(self.hideInTpThr.frame.origin.x +200,self.hideInTpThr.frame.origin.y +200, 260, 300)];
    [self.hideInTpThr addSubview:tpThreeXibtwo];
        tpThreeXibtwo.delegate = self;

}
-(void)didUpdateView:(UIView *)hideView
{
    NSLog(@"kuch to huaa");
}
-(void)hideSideView:(UIGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateEnded)
    {
        [UIView transitionWithView:_hideInTpThr duration:0.5 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [_hideInTpThr setHidden:YES];

            
        } completion:nil];

    }
}
-(void)callTpThreeApi{
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        NSError *error;
        NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:JSON_MAIN_URL]];
        json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{

            jsonArray = [json objectForKey:@"sources"];
            
            jsonArrayId = [jsonArray valueForKey:@"id"];
            jsonArrayName = [jsonArray valueForKey:@"name"];
            
            jsonCategory = [jsonArray valueForKey:@"category"];
//            NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:jsonCategory];
//            jsonCategory = [[NSMutableArray alloc] initWithArray:[mySet array]];
            jsonLanguage = [jsonArray valueForKey:@"language"];
//            NSOrderedSet *mySet1 = [[NSOrderedSet alloc] initWithArray:jsonLanguage];
//            jsonLanguage = [[NSMutableArray alloc] initWithArray:[mySet1 array]];
            jsonCountry = [jsonArray valueForKey:@"country"];
//            NSOrderedSet *mySet2 = [[NSOrderedSet alloc] initWithArray:jsonCountry];
//            jsonCountry = [[NSMutableArray alloc] initWithArray:[mySet2 array]];
            dictCategory = [json objectForKey:@"sources"];
            NSLog(@"%@",dictCategory);
             [self.buttonCollection reloadData];

        });
    });
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (category.length!=0 && language.length ==0 && country.length ==0)
    {
        str2 = [NSString stringWithFormat:@"https://newsapi.org/v2/sources?category=%@&apiKey=326b273af74c4a80913bca7789f2d898",category];
        NSLog(@"%@",str2);
        [self mainShow];
    }
    if (language.length!=0 && category.length ==0 && country.length ==0)
    {
        str2 = [NSString stringWithFormat:@"https://newsapi.org/v2/sources?language=%@&apiKey=326b273af74c4a80913bca7789f2d898",language];
        NSLog(@"%@",str2);
        [self mainShow];

    }
    if (country.length!=0 && category.length ==0 && language.length ==0)
    {
        str2 = [NSString stringWithFormat:@"https://newsapi.org/v2/sources?country=%@&apiKey=326b273af74c4a80913bca7789f2d898",category];
        NSLog(@"%@",str2);
        [self mainShow];
    }
    if(category.length!=0 && language.length!=0 && country.length!=0)
    {
        str2 = [NSString stringWithFormat:@"https://newsapi.org/v2/sources?category=%@&language=%@&country=%@&apiKey=326b273af74c4a80913bca7789f2d898",category,language,country];
        NSLog(@"%@",str2);
        [self mainShow];

    }
    if(category.length ==0 && language.length ==0 && country.length ==0)
    {
        str2 = [NSString stringWithFormat:@"https://newsapi.org/v2/sources?apiKey=326b273af74c4a80913bca7789f2d898"];
        NSLog(@"%@",str2);
        [self mainShow];

    }
//    [self.buttonCollection reloadData];
}

-(void)mainShow{
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        NSError *error;
        NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:str2]];
        json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            jsonArray = [json objectForKey:@"sources"];
            jsonArrayId = [jsonArray valueForKey:@"id"];
            NSLog(@"%@",jsonArrayId);
            jsonArrayName = [jsonArray valueForKey:@"name"];
            NSLog(@"%@",jsonArrayName);
            [self.buttonCollection reloadData];

        });
    });
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length==0){
        isFilter = false;
    }
    else{
        isFilter = true;
        filterDevice = [[NSMutableArray alloc]init];
        for (NSString *device in jsonArrayName) {
            NSRange nameRange = [device rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if (nameRange.location!= NSNotFound){
                NSLog(@"searchText : %@",searchText);
                NSLog(@"Device : %@",device);
                [filterDevice addObject:device];
                NSLog(@"FilterDevice : %@",filterDevice);
                
            }
        }
    }
    [self.buttonCollection reloadData];
}
-(void)didUpdateText:(NSString *)cateText{
    category = [NSString stringWithFormat:@"%@",cateText];
    NSLog(@"%@",category);
}
-(void)didUpdateTextLang:(NSString *)langText
{
    language = [NSString stringWithFormat:@"%@",langText];
    NSLog(@"%@",language);
}
-(void)didUpdateTextCount:(NSString *)countText
{
    country = [NSString stringWithFormat:@"%@",countText];
    NSLog(@"%@",country);
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"fromTpThrToTpTwo"])
    {
        TpTwoViewController *tpTwo= segue.destinationViewController;
        tpTwo.buttonTextt = _buttonId;
    }
//    TpThrToFilter
    if ([segue.identifier isEqualToString:@"TpThrToFilter"])
    {
        FiltersViewController *filterView= segue.destinationViewController;
        filterView.delegate = self;
        filterView.catetext = category;
        filterView.langtext = language;
        filterView.countext = country;
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
   return 1;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Selected section>> %ld",indexPath.section);
    NSLog(@"Selected row of section >> %ld",indexPath.row);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
// return jsonArrayName.count;
    if (isFilter){
        return filterDevice.count;
    }
    else return jsonArrayName.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
//    NSLog(@"Selected section>> %d",indexPath.section);
//    NSLog(@"Selected row of section >> %d",indexPath.row);
    TpThreeCollectionViewCell *cell;
    cell = (TpThreeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"TpThreeCollection" forIndexPath:indexPath];
    
    if (isFilter){
   [cell.buttonn setTitle:[NSString stringWithFormat:@"%@", [filterDevice objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
        [cell.buttonn addTarget:self action:@selector(newVieww:) forControlEvents:UIControlEventAllEvents];
    }
    else{
   [cell.buttonn setTitle:[NSString stringWithFormat:@"%@", [jsonArrayName objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
[cell.buttonn setTag:indexPath.row];
    NSLog(@"%ld",indexPath.row);
    [cell.buttonn addTarget:self action:@selector(newVieww:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
    
}
-(void)newVieww:(id)sender
{
   
    UIButton *buttonn1 = (UIButton *)sender;
  NSLog(@"The button title is %@",buttonn1.titleLabel.text);
//    NSLog(@"tag %ld", (long)buttonn1.tag);
    buttonNum = [jsonArrayName indexOfObject:buttonn1.titleLabel.text];
    NSLog(@"tag %ld", buttonNum);
//    _buttonId =[jsonArrayId objectAtIndex:buttonn1.tag];
//     NSLog(@"%@",[jsonArrayId objectAtIndex:buttonn1.tag]);
    _buttonId =[jsonArrayId objectAtIndex:buttonNum];
    NSLog(@"%@",[jsonArrayId objectAtIndex:buttonNum]);
//    [[SOEventManager sharedEventManager] setSelectedEvent:clickedEvent[0]];
    [self performSegueWithIdentifier:@"fromTpThrToTpTwo" sender:nil];
    
    
}

- (IBAction)backPress:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)filterPress:(id)sender {
    [self performSegueWithIdentifier:@"TpThrToFilter" sender:nil];
}
@end
