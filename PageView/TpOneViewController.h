//
//  TpOneViewController.h
//  News Feed
//
//  Created by 4-OM on 11/20/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TpOneViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
//@property (strong, nonatomic) NSString *strImg;
@property (strong, nonatomic) NSArray *names;
@property (strong, nonatomic) NSArray *data;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView1;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionViewImg;

@property (strong, nonatomic) IBOutlet UILabel *lineUnderImage;

@property (strong, nonatomic) IBOutlet UILabel *lbl_line;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cndt_lineWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cndt_lineXAxis;

@property (strong, nonatomic) IBOutlet UILabel *lblee;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cndt_imgWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cndt_imgXAxis;
@property (strong, nonatomic) UITapGestureRecognizer *singleTap;

@end
