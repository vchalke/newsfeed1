//
//  TpOneNibCollectionViewCell.m
//  News Feed
//
//  Created by 4-OM on 11/26/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "TpOneNibCollectionViewCell.h"

@implementation TpOneNibCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _buttonName.layer.shadowOffset = CGSizeMake(0.0, 2.0);
    _buttonName.layer.masksToBounds = false;
    //   _buttonn.layer.shadowRadius = 2.0;
    _buttonName.layer.shadowOpacity = 0.2;
    
    _buttonName.layer.borderWidth = 1.5f;
    _buttonName.layer.borderColor = [UIColor colorWithRed:255 green:240 blue:250 alpha:1.0].CGColor;
}

@end
