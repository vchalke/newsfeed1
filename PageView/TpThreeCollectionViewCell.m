//
//  TpThreeCollectionViewCell.m
//  News Feed
//
//  Created by 4-OM on 11/23/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "TpThreeCollectionViewCell.h"

@implementation TpThreeCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    [_buttonn setTitleColor:[UIColor pin] forState:UIControlStateNormal];
    _buttonn.layer.shadowOffset = CGSizeMake(0.0, 2.0);
    _buttonn.layer.masksToBounds = false;
//   _buttonn.layer.shadowRadius = 2.0;
    _buttonn.layer.shadowOpacity = 0.2;
    
    _buttonn.layer.borderWidth = 1.5f;
    _buttonn.layer.borderColor = [UIColor colorWithRed:255 green:240 blue:250 alpha:1.0].CGColor;
//    _buttonn.layer.cornerRadius = pressButton.frame.width / 2;
//    _buttonn.layer.borderColor = [UIColor colorWithCGColor:];
    
//    _buttonn.layer.borderWidth = 2.0;
}

@end
