//
//  TpTwoViewController.h
//  News Feed
//
//  Created by 4-OM on 11/22/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TpThreeViewController.h"


@interface TpTwoViewController : UIViewController<UISearchBarDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *imae_view11;
@property (strong, nonatomic) IBOutlet UITextField *txt1table;
@property (strong, nonatomic) IBOutlet UITextField *txt2Table;
@property (strong, nonatomic) IBOutlet UITextField *txt3Table;

@property (strong, nonatomic) IBOutlet UIImageView *imageOnTable;


@property (strong, nonatomic) IBOutlet UICollectionView *imgCollectionView;
@property(nonatomic,retain)NSMutableArray *buttonTextArray;
@property(nonatomic,retain)NSString *buttonTextt;
@property (strong, nonatomic) IBOutlet UITableView *table_View;
@property(nonatomic,retain)NSString *url;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBarr;
@property (strong, nonatomic) IBOutlet UIView *viewInTpTwo;

@property (strong, nonatomic) IBOutlet UIButton *buttonOnImg;
- (IBAction)butOnImgPress:(id)sender;

@end
