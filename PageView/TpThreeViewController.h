//
//  TpThreeViewController.h
//  News Feed
//
//  Created by 4-OM on 11/23/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TpTwoViewController.h"
#import "FiltersViewController.h"
#import "TpThreeXibView1.h"
#import "TpThreeXibTwo.h"

@interface TpThreeViewController :   UIViewController<UISearchBarDelegate,FiltersViewControllerDelegate,TpThreeXibView1Delegate,TpThreeXibTwoDelegate>


@property (strong, nonatomic) IBOutlet UICollectionView *buttonCollection;
@property(nonatomic,retain)NSMutableArray *array;
@property(nonatomic,retain)NSString *buttonId;
@property (strong, nonatomic) IBOutlet UISearchBar *search_Bar;
- (IBAction)filterPress:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *tpTextField;
@property (strong, nonatomic) IBOutlet UIView *tpThreeView1;
@property (strong, nonatomic) IBOutlet UIView *test1;
@property (strong, nonatomic) IBOutlet UIView *hideInTpThr;

@end
