//
//  RootViewController.h
//  News Feed
//
//  Created by 4-OM on 11/17/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "ContentViewController.h"


@interface RootViewController : UIViewController<UIPageViewControllerDataSource,MFMailComposeViewControllerDelegate>
//@interface RootViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *startButtom;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *buttonTitles;
@property (nonatomic)NSInteger currentPage;

@property (strong, nonatomic) NSMutableArray *viewControls;
@property (strong, nonatomic) NSArray *viewArray;

@property (strong, nonatomic) UIViewController *p1;
@property (strong, nonatomic) UIViewController *p2;
@property (strong, nonatomic) UIViewController *p3;


@property (strong, nonatomic) IBOutlet UIView *hidenVieww;


@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UIButton *registerButton;
@property (strong, nonatomic) IBOutlet UIButton *confirmButton;


- (IBAction)loginPressed:(id)sender;
- (IBAction)registerPressed:(id)sender;
- (IBAction)confirmPressed:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *forgotPassButton;
@property (strong, nonatomic) IBOutlet UIButton *notUserButton;

- (IBAction)forgotPassPress:(id)sender;
- (IBAction)notUserPress:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *underLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *underLabelWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *underLabelXAxis;
@end
