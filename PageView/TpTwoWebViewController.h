//
//  TpTwoWebViewController.h
//  News Feed
//
//  Created by 4-OM on 11/26/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface TpTwoWebViewController : UIViewController<UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet WKWebView *webView;
@property(nonatomic,retain)NSString *webUrl;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *actLoading;

@end
