//
//  TpTableViewController.h
//  News Feed
//
//  Created by 4-OM on 11/20/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TpTableViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>  

@property (strong, nonatomic) NSArray *names;
@property (strong, nonatomic) NSArray *data;

@end
