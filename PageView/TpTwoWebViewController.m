//
//  TpTwoWebViewController.m
//  News Feed
//
//  Created by 4-OM on 11/26/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "TpTwoWebViewController.h"

@interface TpTwoWebViewController ()

@end

@implementation TpTwoWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    NSURL *url = [NSURL URLWithString:_webUrl];
   NSLog(@"%@",_webUrl);
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:requestObj];
    [self.view addSubview:_webView];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.actLoading startAnimating];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.actLoading stopAnimating];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backPress:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
