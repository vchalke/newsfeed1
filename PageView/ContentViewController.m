//
//  ContentViewController.m
//  News Feed
//
//  Created by 4-OM on 11/17/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "ContentViewController.h"
#import "LoginViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "AppDelegate.h"
#import "TpViewController.h"
#import <GoogleSignIn/GoogleSignIn.h>
#define EMAIL_ID @"emailid"
#define MAIL_TEXT @"emailid"

static NSString *const kClientId = @"967348977283-crmhqudq4k7hr030s2mkq1tdr1qg4rs8.apps.googleusercontent.com";

@interface ContentViewController ()<FBSDKLoginButtonDelegate,GIDSignInDelegate>
{
    BOOL passwordIsValid;
    BOOL isBOOOL;
}
@end

@implementation ContentViewController


-(NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context=nil;
    _delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if([_delegate respondsToSelector:@selector(persistentContainer)])
    {
        context= _delegate.persistentContainer.viewContext;
    }
    return context;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.titlLabel.text = self.titleText;
    [self.buttonname setTitle:[NSString stringWithFormat:@"%@",self.buttonText] forState:UIControlStateNormal];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
     [GIDSignIn sharedInstance].uiDelegate = self;
}
-(void)dismissKeyboard {
    [self.view endEditing:true];
}
- (void)loginButtonFrame
{
    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] initWithFrame:CGRectMake(0, 0, self.fbookView.frame.size.width, 30)];
    loginButton.readPermissions = @[@"public_profile", @"email"];
    loginButton.delegate = self;
    [self.fbookView addSubview:loginButton];
    if ([FBSDKAccessToken currentAccessToken]){
        [self getFacebookProfileInfos];
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        TpViewController *tpviewController= [storyboard instantiateViewControllerWithIdentifier:@"TpViewController"];
//        tpviewController.labelName = [NSString stringWithFormat:@"%@",self.emailidText.text];
//        [self presentViewController:tpviewController animated:YES completion:nil];

    }

}
-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    NSLog(@"logoutt");
    [self.emailidText setText:[NSString stringWithFormat:@""]];
}
- (void)loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
              error:(NSError *)error
{
    if(result.token)
    {
//        NSLog(@"HApppy bbdy");
//        [self getFacebookProfileInfos];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TpViewController *tpviewController= [storyboard instantiateViewControllerWithIdentifier:@"TpViewController"];
        [self presentViewController:tpviewController animated:YES completion:nil];
    }
    
}
-(void)getFacebookProfileInfos {
    
    FBSDKGraphRequest *requestMe = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:@{@"fields": @"email,name,first_name,last_name"}];
    
    FBSDKGraphRequestConnection *connection = [[FBSDKGraphRequestConnection alloc] init];
    
    [connection addRequest:requestMe completionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        
        if(result)
        {
            if ([result objectForKey:@"email"]) {
                NSLog(@"Email: %@",[result objectForKey:@"email"]);
                [self.emailidText setText:[NSString stringWithFormat:@"%@",[result objectForKey:@"email"]]];
                NSManagedObjectContext *context = [self managedObjectContext];
                [self fetchData];
                if (isBOOOL== NO){
                    NSManagedObject *newContext = [NSEntityDescription insertNewObjectForEntityForName:@"RegData" inManagedObjectContext:context];
                    [newContext setValue:self.emailidText.text forKey:@"emailid"];
                }
                NSError *error = nil;
                if(![context save:&error])
                {
                    NSLog(@"Cant save %@ %@",error,[error localizedDescription]);
                }
            }
        }
        
    }];
    
    [connection start];
}
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    //    [myActivityIndicator stopAnimating];
}
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    if([self.buttonText isEqualToString:@"Login"])
    {
        NSLog(@"Login");
        self.emailidText.text = @"";
        self.passText.text = @"";
        self.buttonname2.hidden = YES;
        [self loginButtonFrame];
    }
    if([self.buttonText isEqualToString:@"Register"])
    {
        NSLog(@"Register");
        self.showPassButton.hidden = YES;
        self.buttonname.hidden = YES;
        [self registrationDesign];
        self.fbookView.hidden =YES;
        self.signInButton.hidden = YES;
    }
    if([self.buttonText isEqualToString:@"Confirm"])
    {
        NSLog(@"Confirm");
        self.passText.hidden = YES;
        self.forgotPassButton.hidden = YES;
        self.notUserButton.hidden = YES;
        self.showPassButton.hidden = YES;
        self.buttonname2.hidden = YES;
        self.fbookView.hidden =YES;
         self.signInButton.hidden = YES;
    }
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"fromlogtomain"])
    {
            NSLog(@"Happy");
    }
}

- (IBAction)buttonPressed:(id)sender {
    
    if([self.buttonText isEqualToString:@"Login"])
    {
        NSLog(@"Login Button");
        [self loginCondition];
        self.buttonname2.hidden = YES;
        [self loginButtonFrame];
    }
    if([self.buttonText isEqualToString:@"Register"])
    {
        NSLog(@"Register Button");
        self.showPassButton.hidden = YES;
        [self registerCondition];
        self.fbookView.hidden =YES;
         self.signInButton.hidden = YES;
        
    }
    if([self.buttonText isEqualToString:@"Confirm"])
    {
        NSLog(@"Confirm Button");
        self.showPassButton.hidden = YES;
        self.fbookView.hidden =YES;
         self.signInButton.hidden = YES;
//        [self blankalertShow];
        self.buttonname2.hidden = YES;
        if ([MFMailComposeViewController canSendMail])
        {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc]init];
        mail.mailComposeDelegate = self;
        [mail setSubject:@"App Feedback"];
        [mail setToRecipients:@[@"test@test.com"]];
        [self presentViewController:mail animated:YES completion:nil];
        }
        else
        {
            NSLog(@"This device cannot send email");
        }
    }
    
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)showPassPress:(id)sender {
    if (self.passText.secureTextEntry == YES) {
        [self.showPassButton setTitle:@"Hide Password" forState:(UIControlStateNormal)];
        self.passText.secureTextEntry = NO;
    }else{
        [self.showPassButton setTitle:@"Show Password" forState:(UIControlStateNormal)];
        self.passText.secureTextEntry = YES;
    }
}

- (IBAction)buttonName2Press:(id)sender {
    NSLog(@"Register Button");
    self.showPassButton.hidden = YES;
    [self registerCondition];
}

//-(void)loginDesign
//{
//    self.forgotPassButton.frame = CGRectMake(57, 650,150, 25);
//    [_forgotPassButton setTitle:@"Forgot Password ?" forState:UIControlStateNormal];
//     [_forgotPassButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal ];
//    [self.view addSubview:_forgotPassButton];
//
//    self.notUserButton.frame = CGRectMake(57, 685,150, 25);
//    [_notUserButton setTitle:@"Not a user ? sign Up" forState:UIControlStateNormal];
//    [self.view addSubview:_notUserButton];
//
//
//}

-(void)registrationDesign
{
    self.emailidText.hidden = YES;
    self.passText.hidden = YES;
    self.showPassButton.hidden = YES;
    self.buttonname.hidden =YES;
//    self.buttonname.frame = CGRectMake(0, 500, 373,47);
//    [self.view addSubview:_buttonname];
    
    self.forgotPassButton.hidden = YES;
    self.notUserButton.hidden = YES;

    self.nametext = [[UITextField alloc] initWithFrame:CGRectMake(30, 100, 360, 30)];
    _nametext.borderStyle = UITextBorderStyleRoundedRect;
    _nametext.font = [UIFont systemFontOfSize:20];
    _nametext.placeholder = @"Name";
    [self.view addSubview:_nametext];

    self.emailtext = [[UITextField alloc] initWithFrame:CGRectMake(30, 145, 360, 30)];
    _emailtext.borderStyle = UITextBorderStyleRoundedRect;
    _emailtext.font = [UIFont systemFontOfSize:20];
    _emailtext.placeholder = @"Email-Id";
    [self.view addSubview:_emailtext];
    
    self.passtext = [[UITextField alloc] initWithFrame:CGRectMake(30, 190, 360, 30)];
    _passtext.borderStyle = UITextBorderStyleRoundedRect;
    _passtext.font = [UIFont systemFontOfSize:20];
    _passtext.placeholder = @"Password";
    [self.view addSubview:_passtext];
    
    self.confpasstext = [[UITextField alloc] initWithFrame:CGRectMake(30, 235, 360, 30)];
    _confpasstext.borderStyle = UITextBorderStyleRoundedRect;
    _confpasstext.font = [UIFont systemFontOfSize:20];
    _confpasstext.placeholder = @"Confirm Password";
    [self.view addSubview:_confpasstext];
    
    self.locationtext = [[UITextField alloc] initWithFrame:CGRectMake(30, 280, 360, 30)];
    _locationtext.borderStyle = UITextBorderStyleRoundedRect;
    _locationtext.font = [UIFont systemFontOfSize:20];
    _locationtext.placeholder = @"Location";
    [self.view addSubview:_locationtext];
    
    self.contacttext = [[UITextField alloc] initWithFrame:CGRectMake(30, 325, 360, 30)];
    _contacttext.borderStyle = UITextBorderStyleRoundedRect;
    _contacttext.font = [UIFont systemFontOfSize:20];
    _contacttext.placeholder = @"Contact";
    [self.view addSubview:_contacttext];
    
//    self.buttonname2.frame = CGRectMake(20, 500,373, 47);
//    self.buttonname2.layer.shadowOpacity = 0.2; self.buttonname2.layer.borderWidth = 1.5f;
//    [self.buttonname2 setTitle:[NSString stringWithFormat:@"Register"] forState:UIControlStateNormal];
//    self.buttonname2.layer.borderColor = [UIColor colorWithRed:255 green:240 blue:250 alpha:1.0].CGColor;
//    [self.buttonname2 addTarget:self action:@selector(registerCondition) forControlEvents:UIControlEventAllEvents];
//    [self.view addSubview:_buttonname];
}


-(void)loginCondition
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"RegData" inManagedObjectContext:managedObjectContext];
    NSError *error = nil;
    [fetchRequest setEntity:entity];
    NSArray *fetchedObj = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if(fetchedObj.count > 0){
        
        
        for (NSManagedObjectContext * infor  in fetchedObj)
        {
            NSString *emailid = [NSString stringWithFormat:@"%@",[infor valueForKey:@"emailid"]];
            NSString *password = [NSString stringWithFormat:@"%@",[infor valueForKey:@"pass"]];
            NSLog(@"%@",emailid);
            NSLog(@"%@",password);
            
            
            
            if ((self.emailidText.text.length != 0)&&
                (self.passText.text.length != 0))
            {
                if ([self.emailidText.text isEqualToString:emailid] &&
                    [self.passText.text isEqualToString:password])
                {
                    [[NSUserDefaults standardUserDefaults]setObject:self.emailidText.text forKey:EMAIL_ID];
                    [self performSegueWithIdentifier:@"fromlogtomain" sender:nil];
                }
                
                
            }
            
            else if (self.emailidText.text.length == 0)
            {
                if ((self.emailidText.text.length == 0)&&
                    (self.passText.text.length == 0))
                {
                    [self blankalertShow];
                }
                else
                    [self blankMailAlert];
                
            }
            
            else if (self.passText.text.length == 0)
            {
                if ((self.emailidText.text.length == 0)&&
                    (self.passText.text.length == 0))
                {
                    [self blankalertShow];
                }
                else
                    [self blankPassAlert];
                
            }
            
            else if ((self.emailidText.text != emailid)&&
                     (self.passText.text == password))
            {
                [self wrongMailAlert];
            }
            
            else if ((self.emailidText.text == emailid)&&
                     (self.passText.text != password))
            {
                [self wrongPassAlert];
            }
            
            //            if ((self.emailid_text.text != emailid)&&
            //                (self.pass_text.text != password))
            //            {
            //                NSLog(@"%@ %@",emailid,password);
            //                [self wrongalertShow];
            //            }
            
            
        }
        
    }
}

-(void)registerCondition
{
    NSManagedObjectContext *context = [self managedObjectContext];
    [self fetchData];
    if (isBOOOL==NO){
        
        if ((self.nametext.text.length != 0)&&
            (self.emailtext.text.length != 0)&&
            ([self.emailtext.text containsString:[NSString stringWithFormat:@"@"]])&&
            ([self.emailtext.text containsString:[NSString stringWithFormat:@"."]])&&
            (self.passtext.text.length != 0)&&
            (self.confpasstext.text.length != 0)&&
            ([self passwordIsValid:self.passtext.text]==YES)&&
            (self.locationtext.text.length != 0)&&
            (self.contacttext.text.length != 0)&&
            ([self.confpasstext.text isEqualToString:self.passtext.text])&&
            (self.contacttext.text.length == 10))
        {
            NSManagedObject *newContext = [NSEntityDescription insertNewObjectForEntityForName:@"RegData" inManagedObjectContext:context];
            [newContext setValue:self.nametext.text forKey:@"name"];
            [newContext setValue:self.emailtext.text forKey:@"emailid"];
            [newContext setValue:self.passtext.text forKey:@"pass"];
            [newContext setValue:self.confpasstext.text forKey:@"confpass"];
            [newContext setValue:self.locationtext.text forKey:@"location"];
            [newContext setValue:self.contacttext.text forKey:@"contact"];
        }
        else [self blankalertShow];
        
    }
    else [self alreadyExist];
    
    [self dataSave];
    NSError *error = nil;
    if(![context save:&error])
    {
        NSLog(@"Cant save %@ %@",error,[error localizedDescription]);
    }
    
}
    

-(void)blankalertShow
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Empty Entry,Mail-Id contaion @ & . ,Both password must be Same , Password Consist atlest 7char, 1numerical,1Capital,1SpecialCharacter " preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Enter Again" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)wrongalertShow
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Wrong Email-Id or Password" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Enter Again" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)wrongMailAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Wrong Email-Id" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Enter Proper Email-Id" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)wrongPassAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Wrong Password" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Enter Proper Password" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)blankMailAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Incomplete Email-Id" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Enter Email-Id" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)blankPassAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Incomplete Password" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Enter Password" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

//-(void)alertView
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Enter Your Details Properly" preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction *Skip = [UIAlertAction actionWithTitle:@"SKIP" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
//                           {
//                           }];
//
//    [alert addAction:Skip];
//
//    [self presentViewController:alert animated:YES completion:nil];
//}

-(void)dataSave
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Data Saved" message:@"saved Data Successfully" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Go back" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                             //                             [self dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)alreadyExist
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Data already Exist" message:@"Use Another Mail-Id" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Go back" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                             //                             [self dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

- (BOOL)passwordIsValid:(NSString *)password {
    
    // 1. Upper case.
    if (![[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:[password characterAtIndex:0]])
        return NO;
    
    // 2. Length.
    if ([password length] < 6)
        return NO;
    
    // 3. Special characters.
    // Change the specialCharacters string to whatever matches your requirements.
    NSString *specialCharacters = @"!#€%&/()[]=?$§@*'";
    if ([[password componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:specialCharacters]] count] < 2)
        return NO;
    
    // 4. Numbers.
    if ([[password componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]] count] < 2)
        return NO;
    
    return YES;
}


-(void)fetchData
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"RegData" inManagedObjectContext:managedObjectContext];
    NSError *error = nil;
    [fetchRequest setEntity:entity];
    NSArray *fetchedObj = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    //    if(fetchedObj.count > 0){
    
    
    for (NSManagedObjectContext * infor  in fetchedObj)
    {
        NSString *emailid = [NSString stringWithFormat:@"%@",[infor valueForKey:@"emailid"]];
        NSString *contact = [NSString stringWithFormat:@"%@",[infor valueForKey:@"contact"]];
        NSLog(@"%@",emailid);
        NSLog(@"%@",contact);
//        if (! [self.emailtext.text isEqualToString:emailid] &&
//            ! [self.contacttext.text isEqualToString:contact])
        if ([self.emailtext.text isEqualToString:emailid] || [self.emailidText.text isEqualToString:emailid] )
        {
            isBOOOL=NO;
            NSLog(@"NO");
            
        }
        else {
            isBOOOL=YES;
            NSLog(@"YES");
            break;
        }
        
        //        }
    }
}

- (IBAction)forgotPassPress:(id)sender {
  
}

- (IBAction)notUserPress:(id)sender {
   
}



@end




//_buttonTitles = @[@"Login", @"Register", @"Confirm"];

