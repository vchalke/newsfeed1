//
//  EditPageViewController.h
//  News Feed
//
//  Created by 4-OM on 11/19/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "RegData+CoreDataClass.h"
#import "RegData+CoreDataProperties.h"
#import "Image+CoreDataClass.h"
#import "Image+CoreDataProperties.h"
#import "AppDelegate.h"
#import "MapInViewController.h"

@interface EditPageViewController : UIViewController<MapInViewControllerDelegate>

@property (strong , nonatomic)NSManagedObject *updateInfo;
@property (nonatomic, strong) NSMutableArray *editPassInfo;
@property (strong , nonatomic)NSManagedObject * info;
@property (nonatomic, strong) UIImagePickerController *imagePicked;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) AppDelegate *delegate;
@property (strong, nonatomic) IBOutlet UIScrollView *scrolInEditPage;
@property (strong, nonatomic) IBOutlet UIView *viewInEditPage;
@property (strong, nonatomic) IBOutlet UIButton *detectLocateButton;
- (IBAction)detectlocPress:(id)sender;


@property (strong, nonatomic) IBOutlet UIImageView *editImageView;
@property (strong, nonatomic) IBOutlet UITextField *name_text_field;
@property (strong, nonatomic) IBOutlet UITextField *emailid_text_field;
@property (strong, nonatomic) IBOutlet UITextField *pass_text_field;
@property (strong, nonatomic) IBOutlet UITextField *locate_text_field;
@property (strong, nonatomic) IBOutlet UITextField *contact_text_field;


- (IBAction)editPhotoPressed:(id)sender;
- (IBAction)updatePress:(id)sender;
- (IBAction)backPress:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *confPass_text_field;

@property (strong , nonatomic) UIView *view1;
@property (strong , nonatomic) UIButton *butt;

@end
