//
//  TpViewController.h
//  News Feed
//
//  Created by 4-OM on 11/20/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreData/CoreData.h>
#import "RegData+CoreDataClass.h"
#import "RegData+CoreDataProperties.h"
#import "Image+CoreDataClass.h"
#import "Image+CoreDataProperties.h"
#import "AppDelegate.h"

@interface TpViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UIPageViewControllerDataSource,UISearchBarDelegate>
@property (strong, nonatomic) UIPageViewController *pageViewController;
//@property (strong, nonatomic) IBOutlet UIImageView *image_view;
@property (strong, nonatomic) IBOutlet UICollectionView *NewCollectionview;
@property (strong, nonatomic) IBOutlet UICollectionView *imgCollectionView;
@property (strong, nonatomic) IBOutlet UICollectionView *buttonCollection;
@property (strong, nonatomic) IBOutlet UIButton *homeOneButton;
@property (strong, nonatomic) IBOutlet UISearchBar *searchInTpView;

@property (strong, nonatomic) IBOutlet UIButton *menuButton;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;
@property (strong, nonatomic) IBOutlet UIButton *editProButton;
@property (strong, nonatomic) IBOutlet UIButton *locateButton;
@property (strong, nonatomic) IBOutlet UIButton *logoutButton;

@property (strong, nonatomic) IBOutlet UIImageView *proImageView;
@property (strong, nonatomic) IBOutlet UILabel *labelName;
@property (strong, nonatomic) IBOutlet UIView *hiddenvieww;
@property (strong, nonatomic) IBOutlet UIView *sidevieww;
@property (strong, nonatomic) IBOutlet UIView *topView;

@property (strong , nonatomic)NSManagedObjectContext * managedObjectContext;
//@property (strong , nonatomic)NSManagedObject * inform;
@property (nonatomic, strong) AppDelegate *delegate;
@property (strong , nonatomic)NSMutableArray * editInfo;

@property(nonatomic,retain)NSMutableArray *array;
@property(nonatomic,retain)NSString *buttonId;
- (IBAction)homeOnePress:(id)sender;

- (IBAction)menuPress:(id)sender;
- (IBAction)homePress:(id)sender;
- (IBAction)editProPress:(id)sender;
- (IBAction)locatePress:(id)sender;
- (IBAction)logoutPress:(id)sender;
@end
