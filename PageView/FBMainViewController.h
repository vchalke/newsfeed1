//
//  FBMainViewController.h
//  News Feed
//
//  Created by 4-OM on 12/12/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
@interface FBMainViewController : UIViewController

@end
