//
//  ContentViewController.h
//  News Feed
//
//  Created by 4-OM on 11/17/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>
#import <CoreData/CoreData.h>
#import "RegData+CoreDataClass.h"
#import "RegData+CoreDataProperties.h"
#import "Image+CoreDataClass.h"
#import "Image+CoreDataProperties.h"
#import "AppDelegate.h"
#import <GoogleSignIn/GoogleSignIn.h>

@interface ContentViewController : UIViewController<MFMailComposeViewControllerDelegate,FBSDKLoginButtonDelegate,GIDSignInDelegate>
@property (strong, nonatomic) IBOutlet UIButton *showPassButton;
@property (strong, nonatomic) IBOutlet GIDSignInButton *signInButton;

@property (nonatomic,strong) id dataObject;
@property (strong, nonatomic) IBOutlet UILabel *titlLabel;
@property (strong, nonatomic) IBOutlet UIButton *buttonname;
- (IBAction)buttonPressed:(id)sender;
- (IBAction)showPassPress:(id)sender;
- (IBAction)buttonName2Press:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *buttonname2;
@property (strong, nonatomic) IBOutlet UITextField *emailidText;
@property (strong, nonatomic) IBOutlet UITextField *passText;
@property (strong, nonatomic) IBOutlet UIView *fbookView;


@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) AppDelegate *delegate;

@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *buttonText;

@property (strong, nonatomic) IBOutlet UITextField *nametext;
@property (strong, nonatomic) IBOutlet UITextField *emailtext;
@property (strong, nonatomic) IBOutlet UITextField *passtext;
@property (strong, nonatomic) IBOutlet UITextField *confpasstext;
@property (strong, nonatomic) IBOutlet UITextField *contacttext;
@property (strong, nonatomic) IBOutlet UITextField *locationtext;

@property (strong, nonatomic) IBOutlet UIButton *forgotPassButton;
@property (strong, nonatomic) IBOutlet UIButton *notUserButton;

- (IBAction)forgotPassPress:(id)sender;
- (IBAction)notUserPress:(id)sender;



@end
