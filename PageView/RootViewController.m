//
//  RootViewController.m
//  News Feed
//
//  Created by 4-OM on 11/17/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController ()



@end

@implementation RootViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    NSString *string1 = @"stringABC";
//    NSString *string2 = @"STRINGDEF";
//    NSComparisonResult result = [string1 caseInsensitiveCompare:string2];
//    
//    if (result == NSOrderedAscending) {
//        NSLog(@"string1 comes before string2");
//    }
//    if (result == NSOrderedSame) {
//        NSLog(@"We're comparing the same string");
//    }
//    if (result == NSOrderedDescending) {
//        NSLog(@"string2 comes before string1");
//    }
    _loginButton.tag=0;
    _registerButton.tag =1;
    _confirmButton.tag = 2;
//    [self logPress];
    _pageTitles = @[@"Login", @"Registration", @"Confirmation"];
    _buttonTitles = @[@"Login", @"Register", @"Confirm"];
    
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    ContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0,90, self.view.frame.size.width, self.view.frame.size.height );
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

//- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
//{
//    ContentViewController *contentViewController = [previousViewControllers objectAtIndex:0];
//    NSUInteger index = contentViewController.pageIndex;
//    NSLog(@"index finished = %lu" , index);
//}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
   
    return 0;
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((ContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    index--;
//    index--;
//        if (index == NSNotFound)) {
//            return nil;
//        }
    return [self viewControllerAtIndex:index];

}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((ContentViewController*) viewController).pageIndex;
    if (index == NSNotFound) {
        return nil;
    }
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
//    index++;
//        if (index == NSNotFound || index == [self.pageTitles count]) {
//            return nil;
//        }
    
    return [self viewControllerAtIndex:index];

}
- (ContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [_pageTitles count])) {
        return nil;
    }

    ContentViewController *contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentView"];
    contentViewController.titleText = self.pageTitles[index];
    contentViewController.buttonText = self.buttonTitles[index];
    contentViewController.pageIndex = index;
    switch (contentViewController.pageIndex) {
        case 0:
            [self logPress:0];
            break;
            case 1:
            break;
            case 2:
            [self logPress:2];
            
        default:
            break;
    }
    return contentViewController;
}
//- (void)pageViewController:(UIPageViewController *)pageViewCntr didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
//{
//    NSInteger currentIndex = ((UIViewController *)self.pageViewController.viewControllers.firstObject).view.tag;
//    if (completed) _pageViewController.currentPage = currentIndex;
//}
- (IBAction)loginPressed:(id)sender {
    ContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
//    [self.loginButton addTarget:self action:@selector(handleTap:) forControlEvents:UIControlEventAllEvents];
    [self logPress:0];
    CGFloat width = 100.00;
    self.underLabelWidth.constant = width;
    self.underLabelXAxis.constant = _loginButton.tag * width;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)registerPressed:(id)sender {
    ContentViewController *startingViewController = [self viewControllerAtIndex:1];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
    self.hidenVieww.hidden=YES;
//    [self regPress];
    [self logPress:1];
    CGFloat width = 105.00;
    self.underLabelWidth.constant = width;
    self.underLabelXAxis.constant = (_registerButton.tag * width);
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
    
}

- (IBAction)confirmPressed:(id)sender {
    ContentViewController *startingViewController = [self viewControllerAtIndex:2];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
   self.hidenVieww.hidden=YES;
//    [self confPress];
    [self logPress:2];
    CGFloat width = 100.00;
    self.underLabelWidth.constant = width;
    self.underLabelXAxis.constant = (_confirmButton.tag * width);
//    self.underLabelXAxis.constant = width;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(void)logPress :(int)index{
    
    
    switch (index) {
        case 0:
            self.loginButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.15f];
            self.registerButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0f];
            self.confirmButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0f];
            break;
        case 1:
            self.registerButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.15f];
            self.loginButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0f];
            self.confirmButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0f];
            break;
        case 2:
            self.confirmButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.15f];
            self.loginButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0f];
            self.registerButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0f];
            break;
        default:
            break;
    }
    
//    self.registerButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0f];
//    self.confirmButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0f];
    
}
//-(void)regPress{
//    self.registerButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.15f];
//    self.loginButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0f];
//    self.confirmButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0f];
    
//}
//-(void)confPress{
//    self.confirmButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.15f];
//    self.loginButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0f];
//    self.registerButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0f];
    
//}
//-(void)tapButton:(id)sender
//{
//
//    UIButton *btn = (UIButton *)sender;
//    NSLog(@"tag %ld", (long)btn.tag);
//    //    CGFloat width = (CGRectGetWidth(self.collectionInRoot.frame)/jsonArrayName.count);
//    CGFloat width = (CGRectGetWidth(self.collectionInRoot.frame)/3);
//    self.underLabelWidth.constant = width;
//    self.underLabelXAxis.constant = btn.tag * width;
//    [UIView animateWithDuration:0.2 animations:^{
//        [self.view layoutIfNeeded];
//    }];
//}
- (IBAction)forgotPassPress:(id)sender {
    ContentViewController *startingViewController = [self viewControllerAtIndex:2];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
    self.hidenVieww.hidden=YES;
}

- (IBAction)notUserPress:(id)sender {
    ContentViewController *startingViewController = [self viewControllerAtIndex:1];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
    self.hidenVieww.hidden=YES;
}

//-(void)handleTap:(id)sender
//{
//    [self.loginButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
//    self.loginButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0f];
//    UIButton *btn = (UIButton *)sender;
//    NSLog(@"tag %ld", (long)btn.tag);
//    CGFloat width = (CGRectGetWidth(self.collectionView1.frame)/jsonArrayTp.count);
//    self.cndt_lineWidth.constant = width;
//    self.cndt_lineXAxis.constant = btn.tag * width;
//    [UIView animateWithDuration:0.2 animations:^{
//        [self.view layoutIfNeeded];
//    }];
//}
@end




/*
- (void)viewDidLoad {
    [super viewDidLoad];

    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageViewController.dataSource = self;
    
    _p1 = [[UIViewController alloc] init];
    _p2 = [[UIViewController alloc] init];
    _p3 = [[UIViewController alloc] init];

    NSUInteger index = 0;
    NSMutableArray *viewControls = [NSMutableArray arrayWithObjects:[self showVCWithIndex:index], nil];
    
    [self.pageViewController setViewControllers:viewControls
                                      direction:UIPageViewControllerNavigationDirectionForward
                                       animated:NO
                                     completion:nil];
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    
    CGRect pageViewRect = self.view.bounds;
    self.pageViewController.view.frame = pageViewRect;
    
    [self.pageViewController didMoveToParentViewController:self];
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
      viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [self.viewArray indexOfObject:viewController];
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self showVCWithIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
       viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [self.viewArray indexOfObject:viewController];
    
    if (index == 2 || index == NSNotFound) {
        return nil;
    }
    
    index++;
    return [self showVCWithIndex:index];
}

- (UIViewController *)showVCWithIndex: (NSUInteger)index
{
    self.viewArray = [NSArray arrayWithObjects: _p1, _p2, _p3, nil];
    
    UIViewController *currentVC = [self.viewArray objectAtIndex:index];
    
    return currentVC;
}
*/


