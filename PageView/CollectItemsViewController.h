//
//  CollectItemsViewController.h
//  News Feed
//
//  Created by 4-OM on 11/19/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreData/CoreData.h>
#import "RegData+CoreDataClass.h"
#import "RegData+CoreDataProperties.h"
#import "Image+CoreDataClass.h"
#import "Image+CoreDataProperties.h"
#import "AppDelegate.h"

@interface CollectItemsViewController : UICollectionViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (strong, nonatomic) IBOutlet UIButton *menuButton;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;
@property (strong, nonatomic) IBOutlet UIButton *editProButton;
@property (strong, nonatomic) IBOutlet UIButton *locateButton;
@property (strong, nonatomic) IBOutlet UIButton *logoutButton;

@property (strong, nonatomic) IBOutlet UIImageView *proImageView;
@property (strong, nonatomic) IBOutlet UILabel *labelName;
@property (strong, nonatomic) IBOutlet UIView *hiddenvieww;
@property (strong, nonatomic) IBOutlet UIView *sidevieww;

@property (strong , nonatomic)NSManagedObjectContext * managedObjectContext;
//@property (strong , nonatomic)NSManagedObject * inform;
@property (nonatomic, strong) AppDelegate *delegate;
@property (strong , nonatomic)NSMutableArray * editInfo;

@property (strong, nonatomic) IBOutlet UIScrollView *scroll_view;
//@property (strong, nonatomic) IBOutlet UIButton *cellButton;


- (IBAction)cellButtonPress:(id)sender;

- (IBAction)menuPress:(id)sender;
- (IBAction)homePress:(id)sender;
- (IBAction)editProPress:(id)sender;
- (IBAction)locatePress:(id)sender;
- (IBAction)logoutPress:(id)sender;


@end
