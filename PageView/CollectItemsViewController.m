//
//  CollectItemsViewController.m
//  News Feed
//
//  Created by 4-OM on 11/19/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "CollectItemsViewController.h"
#import "EditPageViewController.h"
//#import "CollectionViewCell.h"


#define EMAIL_ID @"emailid"
#define MAIL_TEXT @"emailid"

@interface CollectItemsViewController ()
{
    NSMutableArray *devices;
}

@end

@implementation CollectItemsViewController
@synthesize scroll_view;

static NSString * const reuseIdentifier = @"Cell";

-(NSMutableArray *) editInfo
{
    if (! _editInfo)
    {
        _editInfo= [[NSMutableArray alloc] init];
    }
    return _editInfo;
    
}
-(NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context=nil;
    _delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if([_delegate respondsToSelector:@selector(persistentContainer)])
    {
        context= _delegate.persistentContainer.viewContext;
    }
    return context;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   

    
    devices = [[NSMutableArray alloc]initWithObjects:@"aaaa",@"bbbb",@"cccc",@"dddd",@"eeee",@"ffff",@"gggg",nil];
    
//    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
    UIImage *buttonImage = [UIImage imageNamed:@"menuIcon"];
    [_menuButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [self.view addSubview:_menuButton];
    
    UITapGestureRecognizer *tapp = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideSideView:)];
    tapp.numberOfTapsRequired = 1;
    [_sidevieww addGestureRecognizer:tapp];
    self.labelName.text=[[NSUserDefaults standardUserDefaults]objectForKey:EMAIL_ID];

    
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"image"];
    UIImage* image = [UIImage imageWithData:imageData];
    self.proImageView.image = image;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"image"];
    UIImage* image = [UIImage imageWithData:imageData];
    self.proImageView.image = image;
    
    self.labelName.text=[[NSUserDefaults standardUserDefaults]objectForKey:MAIL_TEXT];
    NSLog(@"%@",self.labelName.text);
    
    UIImage *buttonImage = [UIImage imageNamed:@"menuIcon"];
    [_menuButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [self.view addSubview:_menuButton];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest=[[NSFetchRequest alloc]initWithEntityName:@"RegData"];
    self.editInfo=[[managedObjectContext executeFetchRequest:fetchRequest error:nil]mutableCopy];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)hideSideView:(UIGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateEnded)
    {
        UIImage *buttonImage = [UIImage imageNamed:@"menuIcon"];
        [_menuButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
        [self.view addSubview:_menuButton];
        [_hiddenvieww setHidden:YES];
        [UIView transitionWithView:_sidevieww duration:0.2 options:UIViewAnimationOptionCurveEaseIn animations:^{
            CGRect frame = _sidevieww.frame;
            frame.origin.x=-_sidevieww.frame.size.width;
            _sidevieww.frame = frame;
            
        } completion:nil];
    }
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"fromcollecttoedit"])
    {
        
        [self viewDidAppear:YES];
        
        for (NSManagedObject * info  in _editInfo){
            NSString *emailid = [NSString stringWithFormat:@"%@",[info valueForKey:@"emailid"]];  
            if ([emailid isEqualToString:self.labelName.text]){
                EditPageViewController *editProfile= segue.destinationViewController;
                editProfile.updateInfo = info;
                
            }
        }
    }
}

/*

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
//#warning Incomplete implementation, return the number of sections
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of items
    return devices.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionView *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell
//    [cell.collectionlabel setText:[NSString stringWithFormat:@"%@",devices[indexPath.row]]];
//    UIImageView *image1 = (UIImageView *)[cell viewWithTag:1];
//    image1.image = [UIImage imageNamed:[devices objectAtIndex:indexPath.row]];
    
    UIButton *cellButton = (UIButton *)[cell viewWithTag:1];
    [cellButton setTitle:[devices objectAtIndex:indexPath.row] forState:UIControlStateNormal];
    return cell;
}


*/

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

- (IBAction)cellButtonPress:(id)sender {
}

- (IBAction)menuPress:(id)sender {
    [_hiddenvieww setHidden:NO];
    UIImage *buttonImage = [UIImage imageNamed:@"backIcon"];
    [_menuButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [self.view addSubview:_menuButton];
    [UIView transitionWithView:_sidevieww duration:0.2 options:UIViewAnimationOptionCurveEaseIn animations:^{
        CGRect frame = _sidevieww.frame;
        frame.origin.x=0;
        _sidevieww.frame = frame;
        
        
    } completion:nil];
}

- (IBAction)homePress:(id)sender {
}

- (IBAction)editProPress:(id)sender {
    [self performSegueWithIdentifier:@"fromcollecttoedit" sender:nil];
}

- (IBAction)locatePress:(id)sender {
}

- (IBAction)logoutPress:(id)sender {
     [self dismissViewControllerAnimated:YES completion:nil];
}


@end
