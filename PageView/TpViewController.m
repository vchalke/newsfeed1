//
//  TpViewController.m
//  News Feed
//
//  Created by 4-OM on 11/20/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "TpViewController.h"
#import "EditPageViewController.h"
#import "TpOneNibCollectionViewCell.h"
#import "TpViewNibCollectionViewCell.h"
#import "RootPagerContentViewController.h"
#import "RootViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#define EMAIL_ID @"emailid"
#define MAIL_TEXT @"emailid"
#define JSON_FILE_URL @"https://newsapi.org/v2/sources?apiKey=326b273af74c4a80913bca7789f2d898"
#define JSON_FILE_URL_IMG @"https://newsapi.org/v2/top-headlines?sources=abc-news&apiKey=326b273af74c4a80913bca7789f2d898"

@interface TpViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,FBSDKLoginButtonDelegate>
{
    NSMutableArray *devices;
    NSMutableDictionary *json ;
    NSMutableArray *jsonArray ;
    NSMutableArray *jsonArrayId ;
    NSMutableArray *jsonId ;
    NSMutableArray *jsonArrayName ;
    NSMutableArray *jsonArrayUrl ;
    NSMutableArray *namess ;
    NSDictionary *items;
    
    NSMutableDictionary *json1 ;
    NSMutableArray *jsonImg ;
    NSMutableArray *jsonUrlImg ;
    
    NSMutableArray *filterDevice;
    BOOL isFilter;
    NSInteger butNumm;
}

@end

@implementation TpViewController

-(NSMutableArray *) editInfo
{
    if (! _editInfo)
    {
        _editInfo= [[NSMutableArray alloc] init];
    }
    return _editInfo;
    
}
-(NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context=nil;
    _delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if([_delegate respondsToSelector:@selector(persistentContainer)])
    {
        context= _delegate.persistentContainer.viewContext;
    }
    return context;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.searchInTpView.delegate = self;
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewControl"];
    self.pageViewController.dataSource = self;

    [self.buttonCollection registerNib:[UINib nibWithNibName:@"TpOneNibCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TpOneNibCollection"];

    [self.NewCollectionview registerNib:[UINib nibWithNibName:@"TpViewNibCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TpViewNibCollection"];

    [self callTpViewApi];
//    UIImage *buttonImage = [UIImage imageNamed:@"buttonmenuu"];
//    [self.menuButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
//    [self.view addSubview:_menuButton];
    
    UITapGestureRecognizer *tapp = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideSideView:)];
    tapp.numberOfTapsRequired = 1;
    [_sidevieww addGestureRecognizer:tapp];
    self.labelName.text=[[NSUserDefaults standardUserDefaults]objectForKey:EMAIL_ID];
    UITapGestureRecognizer *tapKey = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tapKey];
    
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"image"];
    UIImage* image = [UIImage imageWithData:imageData];
    self.proImageView.image = image;

}

-(void)dismissKeyboard {
    [self.topView endEditing:true];
     [self.view endEditing:true];
    
}
-(void)callTpViewApi{
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        NSError *error;
        NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:JSON_FILE_URL]];
        json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            jsonArray = [json objectForKey:@"sources"];
            jsonArrayId = [jsonArray valueForKey:@"id"];
            jsonArrayName = [jsonArray valueForKey:@"name"];
            jsonId = [jsonArray valueForKey:@"id"];
            RootPagerContentViewController *startingView = [self viewControllerAtIndex:0];
            NSArray *viewControllers = @[startingView];
            [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
            
            self.pageViewController.view.frame = CGRectMake(0,160, self.view.frame.size.width, self.view.frame.size.height );
            
            [self addChildViewController:_pageViewController];
            [self.view addSubview:_pageViewController.view];
            [self.pageViewController didMoveToParentViewController:self];
            
            [self.NewCollectionview reloadData];
            
        });
    });
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     [_hiddenvieww setHidden:YES];
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"image"];
    UIImage* image = [UIImage imageWithData:imageData];
    self.proImageView.image = image;
    
    self.labelName.text=[[NSUserDefaults standardUserDefaults]objectForKey:MAIL_TEXT];
    NSLog(@"%@",self.labelName.text);
    
    UIImage *buttonImage = [UIImage imageNamed:@"buttonmenuu"];
//    UIImage *buttonImage = [UIImage imageNamed:@"menu (1)"];
    [self.menuButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [self.view addSubview:_menuButton];
    self.pageViewController.view.hidden = NO;
    [self.searchInTpView setHidden:NO];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest=[[NSFetchRequest alloc]initWithEntityName:@"RegData"];
    self.editInfo=[[managedObjectContext executeFetchRequest:fetchRequest error:nil]mutableCopy];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)hideSideView:(UIGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateEnded)
    {
//        buttonmenuu
         UIImage *buttonImage = [UIImage imageNamed:@"buttonmenuu"];
//        UIImage *buttonImage = [UIImage imageNamed:@"menu (1)"];
        [self.menuButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
        [self.view addSubview:_menuButton];
        [UIView transitionWithView:_sidevieww duration:0.5 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [_hiddenvieww setHidden:YES];
            CGRect frame = _sidevieww.frame;
            frame.origin.x=-_sidevieww.frame.size.width;
            _sidevieww.frame = frame;

        } completion:nil];
        self.pageViewController.view.hidden =NO;
        self.searchInTpView.hidden =NO;
    }
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length==0){
        isFilter = false;
    }
    else{
        isFilter = true;
        filterDevice = [[NSMutableArray alloc]init];
        for (NSString *device in jsonArrayName) {
            NSRange nameRange = [device rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if (nameRange.location!= NSNotFound){
                [filterDevice addObject:device];
                
            }
        }
    }
    [self.NewCollectionview reloadData];
}
- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [jsonArrayName count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    
    return 0;
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((RootPagerContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    
    index--;
    NSLog(@"Before %ld",index);
    if (index ==0){
        //        [self regPress];
    }
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((RootPagerContentViewController*) viewController).pageIndex;
    //    NSLog(@"After %ld",index);
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    
    NSLog(@"After %ld",index);
    if (index == [jsonArrayName count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}
- (RootPagerContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    //    NSUInteger index = ((ContentViewController*) viewController).pageIndex;
    if (([jsonArrayName count] == 0) || (index >= [jsonArrayName count])) {
        return nil;
    }
    
    RootPagerContentViewController *rootPagerContent = [self.storyboard instantiateViewControllerWithIdentifier:@"rootPagerContent"];
    rootPagerContent.titleText = jsonArrayName[index];
    NSLog(@"%@",rootPagerContent.titleText);
    rootPagerContent.pageIndex = index;
    NSLog(@"%ld",rootPagerContent.pageIndex);
    
    NSInteger numTitle =[jsonArrayName indexOfObject:rootPagerContent.titleText];
    NSLog(@"%ld",numTitle);
    NSString *api3 = [jsonId objectAtIndex:numTitle];
    NSString *topNewsApi = [NSString stringWithFormat:@"https://newsapi.org/v2/top-headlines?sources=%@&apiKey=326b273af74c4a80913bca7789f2d898",api3];
    NSLog(@"%@",topNewsApi);
    rootPagerContent.tableViewApi = topNewsApi;
    
    return rootPagerContent;
    
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"fromcollecttoedit"])
    {
        
        [self viewDidAppear:YES];
        
        for (NSManagedObject * info  in _editInfo){
            NSString *emailid = [NSString stringWithFormat:@"%@",[info valueForKey:@"emailid"]];
            if ([emailid isEqualToString:self.labelName.text]){
                EditPageViewController *editProfile= segue.destinationViewController;
                editProfile.updateInfo = info;
                
            }
        }
    }
}



- (IBAction)menuPress:(id)sender {
//    [_hiddenvieww setHidden:NO];
    
    UIImage *buttonImage = [UIImage imageNamed:@"leftarrow"];
    [self.menuButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [self.view addSubview:_menuButton];
    
    [UIView transitionWithView:_sidevieww duration:0.5 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [_hiddenvieww setHidden:NO];
        CGRect frame = _sidevieww.frame;
        frame.origin.x=0;
        _sidevieww.frame = frame;
        self.pageViewController.view.hidden =YES;
        self.searchInTpView.hidden =YES;
    } completion:nil];

//    self.pageViewController.view.hidden =YES;
}
- (IBAction)homeOnePress:(id)sender {
   //   [self performSegueWithIdentifier:@"FromTpViewToRootPagerView" sender:nil];
    [self.menuButton setBackgroundImage:[UIImage imageNamed:@"buttonmenuu"] forState:UIControlStateNormal];
    [self.view addSubview:_menuButton];
    [UIView transitionWithView:_sidevieww duration:0.5 options:UIViewAnimationOptionCurveEaseOut animations:^{
    self.hiddenvieww.hidden=YES;
        } completion:nil];
    self.pageViewController.view.hidden =NO;
    self.searchInTpView.hidden =NO;
    self.NewCollectionview.hidden = NO;
    
}
- (IBAction)homePress:(id)sender {
     [self performSegueWithIdentifier:@"fromcollecttotpthree" sender:nil];
//    fromcollecttoedit
}
//FromTpViewToRootPagerView
- (IBAction)editProPress:(id)sender {
    if ([FBSDKAccessToken currentAccessToken]){
        [self alert];
    }
    else [self performSegueWithIdentifier:@"fromcollecttoedit" sender:nil];
}

- (IBAction)locatePress:(id)sender {
   [self performSegueWithIdentifier:@"FromTpViewToMapp" sender:nil];
}

- (IBAction)logoutPress:(id)sender {
    
    if ([FBSDKAccessToken currentAccessToken]){
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logOut];
    [FBSDKAccessToken setCurrentAccessToken:nil];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RootViewController *rootviewController= [storyboard instantiateViewControllerWithIdentifier:@"RootViewController"];
    [self presentViewController:rootviewController animated:YES completion:nil];
    }
    else [self dismissViewControllerAnimated:YES completion:nil];
}



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (isFilter){
        return filterDevice.count;
    }
    return jsonArrayName.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
        TpViewNibCollectionViewCell *cell;
        cell = (TpViewNibCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"TpViewNibCollection" forIndexPath:indexPath];
    if (isFilter){
        [cell.nibButton setTitle:[NSString stringWithFormat:@"%@", [filterDevice objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
        [cell.nibButton addTarget:self action:@selector(afterFilter:) forControlEvents:UIControlEventAllEvents];
    }
    else{
    [cell.nibButton setTitle:[NSString stringWithFormat:@"%@", [jsonArrayName objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
    [cell.nibButton setTag:indexPath.row];
    NSLog(@"%ld",indexPath.row);
    [cell.nibButton addTarget:self action:@selector(tapButton:) forControlEvents:UIControlEventTouchUpInside];
    }
     return cell;
    
}
-(void)tapButton:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;
//    [btn setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
    NSLog(@"tag %ld", (long)btn.tag);
//    CGFloat width = (CGRectGetWidth(self.collectionInRoot.frame)/3);
//    self.underLabelWidth.constant = width;
//    self.underLabelXAxis.constant = btn.tag * width;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
    RootPagerContentViewController *startingRootPager = [self viewControllerAtIndex:btn.tag];
    NSArray *viewControllers = @[startingRootPager];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
    //    [self viewControllerAtIndex:btn.tag];
}
-(void)afterFilter:(id)sender
{
    UIButton *butn1 = (UIButton *)sender;
    NSLog(@"The button title is %@",butn1.titleLabel.text);
//    NSLog(@"tag %ld", (long)butn1.tag);
    butNumm = [jsonArrayName indexOfObject:butn1.titleLabel.text];
    NSLog(@"tag %ld", butNumm);
    RootPagerContentViewController *startingRootPager = [self viewControllerAtIndex:butNumm];
    NSArray *viewControllers = @[startingRootPager];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
}
//-(void)newVieww:(UIButton*)sender
//-(void)newVieww:(id)sender
//{
//    NSLog(@"Happy");
//    [self alert];
//    UIButton *buttonn1 = (UIButton *)sender;
//    NSLog(@"tag %ld", (long)buttonn1.tag);
//    _buttonId =[jsonArrayId objectAtIndex:buttonn1.tag];
//    NSLog(@"%@",[jsonArrayId objectAtIndex:buttonn1.tag]);
//    [self performSegueWithIdentifier:@"fromTpThrToTpTwo" sender:nil];
    
//}
//- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

//    static NSString *reuseIdentifier = @"Cell";
//    UICollectionViewCell *cell1 = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
//    UIButton *collButton = (UIButton *)[cell1 viewWithTag:1];
//    [collButton setTitle:[NSString stringWithFormat:@"%@", [jsonArrayId objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
//    return cell1;
    
//}
-(void)alert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Info Is Not Loaded" message:@"Login Through App" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Cancle" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                         }];

    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
