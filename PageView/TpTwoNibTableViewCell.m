//
//  TpTwoNibTableViewCell.m
//  News Feed
//
//  Created by 4-OM on 12/1/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "TpTwoNibTableViewCell.h"

@implementation TpTwoNibTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
