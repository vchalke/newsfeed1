//
//  EditPageViewController.m
//  News Feed
//
//  Created by 4-OM on 11/19/18.
//  Copyright © 2018 Techathalon. All rights reserved.
//

#import "EditPageViewController.h"
#import "MapInViewController.h"

#define EMAIL_ID @"emailid"
#define MAIL_TEXT @"emailid"

@interface EditPageViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,MapInViewControllerDelegate>
{
    BOOL infoIsValid;
    BOOL updateBOOL;
    NSString *ordinateText;
}

@end

@implementation EditPageViewController
@synthesize updateInfo;

-(NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context=nil;
    _delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if([_delegate respondsToSelector:@selector(persistentContainer)])
    {
        context= _delegate.persistentContainer.viewContext;
    }
    return context;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
     [self.scrolInEditPage setContentSize:CGSizeMake(414, 850)];
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"image"];
    UIImage* image = [UIImage imageWithData:imageData];
    self.editImageView.image = image;
    
    
    if (self.updateInfo)
    {
        [self.name_text_field setText:[self.updateInfo valueForKey:@"name"]];
        [self.emailid_text_field setText:[self.updateInfo valueForKey:@"emailid"]];
        [self.pass_text_field setText:[self.updateInfo valueForKey:@"pass"]];
        [self.confPass_text_field setText:[self.updateInfo valueForKey:@"confpass"]];
        [self.locate_text_field setText:[self.updateInfo valueForKey:@"location"]];
        [self.contact_text_field setText:[self.updateInfo valueForKey:@"contact"]];
    }
}
-(void)dismissKeyboard {
    [self.view endEditing:true];
}
-(void)didUpdateText:(NSString *)coOrdinate{
    ordinateText = [NSString stringWithFormat:@"%@",coOrdinate];
    NSLog(@"%@",ordinateText);
    self.locate_text_field.text = [NSString stringWithFormat:@"%@",ordinateText];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"FromDetectToMapInView"])
    {
        MapInViewController *mapInViewPage= segue.destinationViewController;
        mapInViewPage.delegate = self;
    }
}


- (IBAction)editPhotoPressed:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    [self presentViewController:picker animated:YES completion : nil];
}

- (IBAction)updatePress:(id)sender {
    
    NSManagedObjectContext *context = [self managedObjectContext];

    if (self.updateInfo){
        if ((self.name_text_field.text.length!=0)&&
            (self.emailid_text_field.text.length!=0)&&
            (self.pass_text_field.text.length!=0)&&
            (self.confPass_text_field.text.length!=0)&&
            (self.locate_text_field.text.length!=0)&&
            (self.contact_text_field.text.length==10)&&
            ([self.pass_text_field.text isEqualToString:self.confPass_text_field.text])&&
            ([self.emailid_text_field.text containsString:[NSString stringWithFormat:@"@"]])&&
            ([self.emailid_text_field.text containsString:[NSString stringWithFormat:@"."]])&&
            ([self infoIsValid:self.pass_text_field.text]==YES)   )
        {
            [self.updateInfo setValue:self.name_text_field.text forKey:@"name" ];
            [self.updateInfo setValue:self.emailid_text_field.text forKey:@"emailid" ];
            [self.updateInfo setValue:self.pass_text_field.text forKey:@"pass" ];
            [self.updateInfo setValue:self.confPass_text_field.text forKey:@"confpass" ];
            [self.updateInfo setValue:self.locate_text_field.text forKey:@"location" ];
            [self.updateInfo setValue:self.contact_text_field.text forKey:@"contact" ];
        }
        else [self showAlert];
    }
    else [self showAlert];
    [[NSUserDefaults standardUserDefaults]setObject:self.emailid_text_field.text forKey:MAIL_TEXT];
    
    NSError *error = nil;
    if(![context save:&error])
    {
        NSLog(@"Cant save %@ %@",error,[error localizedDescription]);
    }
    [self dataSaved];
//    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)backPress:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)detectlocPress:(id)sender {
//    FromDetectToMapInView
    [self performSegueWithIdentifier:@"FromDetectToMapInView" sender:nil];
//    self.view1.frame = CGRectMake(0, 90, self.view.frame.size.width, self.view.frame.size.height);
//    [self.viewInEditPage addSubview:_view1];
//    self.butt.frame = CGRectMake(100, 100, self.view.frame.size.width-100,100);
//    self.butt.backgroundColor = [UIColor redColor];
//    [self.butt setTitle:@"Press" forState:UIControlStateNormal];
//    [self.butt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [self.butt addTarget:self action:@selector(buttTap) forControlEvents:UIControlEventTouchUpInside];
//    [self.viewInEditPage addSubview:_butt];
}
//-(void)buttTap
//{
//    self.view1.hidden = YES;
//}
#pragma mark UIImagePickerControllerDelegate
///////////////////////////////////////////

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    if (! image){
        image = info[UIImagePickerControllerOriginalImage];
    }
    if (image){
        self.editImageView.image = image;
        
        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(image) forKey:@"image"];
    }
    
    //    [self.photos addObject : [self photoFromImage:image]];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    NSLog(@"Cancel");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)infoIsValid:(NSString *)password {
    
    // 1. Upper case.
    if (![[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:[password characterAtIndex:0]])
        return NO;
    
    // 2. Length.
    if ([password length] < 6)
        return NO;
    
    // 3. Special characters.
    // Change the specialCharacters string to whatever matches your requirements.
    NSString *specialCharacters = @"!#€%&/()[]=?$§@*'";
    if ([[password componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:specialCharacters]] count] < 2)
        return NO;
    
    // 4. Numbers.
    if ([[password componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]] count] < 2)
        return NO;
    
    return YES;
}
-(void)showAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"7Char,1Num,1Key,1Capital,valid mailid,valid contact,fill all info,pass & confirm pass is exactly same" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Try Again" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)dataSaved
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Data Saved Successfully" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Go back" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)alreadyExist
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Data already Exist" message:@"Use Another Mail-Id or Contact" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Go back" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                         {
                             //                             [self dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
